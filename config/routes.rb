Rails.application.routes.draw do
  get 'google/callback'

  get 'google/redirect'

  get 'google/jobs'

  resources :configurations
  resources :app_configurations
  get 'taghere/menu'

  get 'taghere/set_table'
  get 'taghere/order'
  get 'taghere/balance'
  get 'taghere/garcon'
  get 'taghere/success'

  resources :employees do
    member do
      get "adiantamento"
    end
    collection do
      get "imprimir_adiantamento"
      get "deletar_vale"
    end
  end
  resources :couverts
  get 'garcon/chamadas'

  get 'garcon/pedido'

  get 'garcon/fechar_conta'

  devise_for :users
  resources :users
  resources :cancel_reasons
  resources :measures
  resources :pedido_extras
  resources :extras
  resources :payment_types
  resources :client_phones
  resources :clients do
    collection do
      get "check_cpf"
      get "credito"
    end
    member do
      get "crediario"
    end
  end
  resources :providers do
    collection do
      get "check_cpf"
    end
  end
  resources :ingredient_entraces
  resources :entrances
  resources :comandas do
    member do
      get "novo_pedido"
      get "concluir"
      get "pedido_simples"
      get "imprimir"
      get "imprimir_nogroup"
      get "calcular_taxa"
      get "inserir_couvert"
      get "fechar"
      get "calcular_desconto"

    end
    collection do
      get "enviar_pedido"
      get "dashboard"
      get "simples"
      get "caixa"
    end
  end
  resources :flavor_orders
  resources :product_orders do
    collection do
      get "atualiza_pedido"
    end
    member do
      get "change_comanda"
      get "cancelar"
      get "imprimir"
      get "concluir"
    end
  end
  resources :orders do
    member do
      get "change_comanda"
      get "cancelar"
      get "print"
    end
  end
  resources :dining_tables do
    collection do
      get "home"
    end
    member do
      get "comandas"
      get "unificar_comandas"
    end
  end
  resources :promotion_flavor_sizes
  resources :promotions
  resources :flavor_sizes do
    collection do
      get "produtos_por_categoria"
      get "retorna_sabores"
      get "retorna_adicionais"
    end
  end
  resources :ingredient_flavors
  resources :flavors
  resources :product_sizes
  resources :products
  resources :areas do
    collection do
      get "atualiza"
    end
  end
  resources :subgroups
  resources :product_groups
  resources :ingredients

  resources :sage_one do
    collection do
      get "url_callback"
    end
  end

  root "dining_tables#home"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
