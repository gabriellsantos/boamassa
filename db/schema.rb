# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180226180643) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "areas", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "printer"
  end

  create_table "cancel_reasons", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "client_payments", force: :cascade do |t|
    t.bigint "client_id"
    t.decimal "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_client_payments_on_client_id"
  end

  create_table "client_phones", force: :cascade do |t|
    t.string "phone"
    t.bigint "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_client_phones_on_client_id"
  end

  create_table "clients", force: :cascade do |t|
    t.string "nome"
    t.string "cpf_cnpj"
    t.string "inscricao_estadual"
    t.string "nome_fantasia"
    t.string "cep"
    t.string "endereco"
    t.string "bairro"
    t.string "complemento"
    t.string "cidade"
    t.string "estado"
    t.string "email"
    t.string "observacoes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "taghere_uid"
    t.string "taghere_token"
    t.string "contaazul_id"
  end

  create_table "comandas", force: :cascade do |t|
    t.bigint "dining_table_id"
    t.string "tipo"
    t.boolean "entrega"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "status"
    t.bigint "client_id"
    t.decimal "couvert_value"
    t.integer "couvert_quantity"
    t.decimal "tax"
    t.boolean "fechada"
    t.string "contaazul_id"
    t.decimal "desconto"
    t.integer "who_close"
    t.index ["client_id"], name: "index_comandas_on_client_id"
    t.index ["dining_table_id"], name: "index_comandas_on_dining_table_id"
  end

  create_table "configurations", force: :cascade do |t|
    t.string "key"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "couverts", force: :cascade do |t|
    t.string "name"
    t.decimal "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dining_tables", force: :cascade do |t|
    t.string "number"
    t.string "beacon"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "down_payments", force: :cascade do |t|
    t.bigint "employee_id"
    t.decimal "value"
    t.date "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "note"
    t.index ["employee_id"], name: "index_down_payments_on_employee_id"
  end

  create_table "employees", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "pis"
    t.string "phone"
    t.string "cpf"
    t.string "rg"
    t.date "hiring_date"
    t.date "demission_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "entrances", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "provider_id"
    t.index ["provider_id"], name: "index_entrances_on_provider_id"
  end

  create_table "extras", force: :cascade do |t|
    t.bigint "product_size_id"
    t.bigint "flavor_id"
    t.decimal "value"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["flavor_id"], name: "index_extras_on_flavor_id"
    t.index ["product_size_id"], name: "index_extras_on_product_size_id"
  end

  create_table "flavor_orders", force: :cascade do |t|
    t.bigint "product_order_id"
    t.bigint "flavor_size_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["flavor_size_id"], name: "index_flavor_orders_on_flavor_size_id"
    t.index ["product_order_id"], name: "index_flavor_orders_on_product_order_id"
  end

  create_table "flavor_sizes", force: :cascade do |t|
    t.bigint "flavor_id"
    t.bigint "product_size_id"
    t.decimal "value"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "visible"
    t.index ["flavor_id"], name: "index_flavor_sizes_on_flavor_id"
    t.index ["product_size_id"], name: "index_flavor_sizes_on_product_size_id"
  end

  create_table "flavors", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "product_id"
    t.decimal "value"
    t.boolean "active"
    t.boolean "visible"
    t.index ["product_id"], name: "index_flavors_on_product_id"
  end

  create_table "ingredient_entraces", force: :cascade do |t|
    t.bigint "entrance_id"
    t.bigint "ingredient_id"
    t.decimal "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "quantity"
    t.index ["entrance_id"], name: "index_ingredient_entraces_on_entrance_id"
    t.index ["ingredient_id"], name: "index_ingredient_entraces_on_ingredient_id"
  end

  create_table "ingredient_flavors", force: :cascade do |t|
    t.bigint "ingredient_id"
    t.bigint "flavor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "quantity"
    t.index ["flavor_id"], name: "index_ingredient_flavors_on_flavor_id"
    t.index ["ingredient_id"], name: "index_ingredient_flavors_on_ingredient_id"
  end

  create_table "ingredients", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "quantity"
    t.bigint "measure_id"
    t.index ["measure_id"], name: "index_ingredients_on_measure_id"
  end

  create_table "measures", force: :cascade do |t|
    t.string "name"
    t.string "sigla"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "order_payments", force: :cascade do |t|
    t.bigint "payment_type_id"
    t.decimal "value"
    t.bigint "client_id"
    t.boolean "paid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "comanda_id"
    t.index ["client_id"], name: "index_order_payments_on_client_id"
    t.index ["comanda_id"], name: "index_order_payments_on_comanda_id"
    t.index ["payment_type_id"], name: "index_order_payments_on_payment_type_id"
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "comanda_id"
    t.string "status"
    t.time "delivery_time"
    t.time "delivery_duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "cancel_reason_id"
    t.bigint "user_id"
    t.index ["cancel_reason_id"], name: "index_orders_on_cancel_reason_id"
    t.index ["comanda_id"], name: "index_orders_on_comanda_id"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "payment_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pedido_extras", force: :cascade do |t|
    t.bigint "product_order_id"
    t.bigint "extra_id"
    t.decimal "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["extra_id"], name: "index_pedido_extras_on_extra_id"
    t.index ["product_order_id"], name: "index_pedido_extras_on_product_order_id"
  end

  create_table "product_groups", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_orders", force: :cascade do |t|
    t.bigint "order_id"
    t.string "note"
    t.string "status"
    t.decimal "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "flavor_size_id"
    t.decimal "quantity"
    t.bigint "cancel_reason_id"
    t.string "tempo"
    t.bigint "user_id"
    t.index ["cancel_reason_id"], name: "index_product_orders_on_cancel_reason_id"
    t.index ["flavor_size_id"], name: "index_product_orders_on_flavor_size_id"
    t.index ["order_id"], name: "index_product_orders_on_order_id"
    t.index ["user_id"], name: "index_product_orders_on_user_id"
  end

  create_table "product_sizes", force: :cascade do |t|
    t.bigint "product_id"
    t.string "name"
    t.string "description"
    t.integer "quantity_flavors"
    t.decimal "value"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_product_sizes_on_product_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "photo"
    t.bigint "subgroup_id"
    t.bigint "area_id"
    t.string "calculo"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["area_id"], name: "index_products_on_area_id"
    t.index ["subgroup_id"], name: "index_products_on_subgroup_id"
  end

  create_table "promotion_flavor_sizes", force: :cascade do |t|
    t.decimal "discount"
    t.bigint "promotion_id"
    t.bigint "flavor_size_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["flavor_size_id"], name: "index_promotion_flavor_sizes_on_flavor_size_id"
    t.index ["promotion_id"], name: "index_promotion_flavor_sizes_on_promotion_id"
  end

  create_table "promotion_groups", force: :cascade do |t|
    t.bigint "subgroup_id"
    t.bigint "promotion_id"
    t.decimal "discount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["promotion_id"], name: "index_promotion_groups_on_promotion_id"
    t.index ["subgroup_id"], name: "index_promotion_groups_on_subgroup_id"
  end

  create_table "promotions", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "start_at"
    t.datetime "end_at"
  end

  create_table "providers", force: :cascade do |t|
    t.string "name"
    t.string "inscricao"
    t.string "inscricao_estadual"
    t.string "nome_fantasia"
    t.string "cep"
    t.string "endereco"
    t.string "bairro"
    t.string "cidade"
    t.string "estado"
    t.string "email"
    t.string "observacoes"
    t.string "telefones"
    t.string "site"
    t.boolean "ativo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "numero"
    t.string "complemento"
  end

  create_table "subgroups", force: :cascade do |t|
    t.string "name"
    t.bigint "product_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_group_id"], name: "index_subgroups_on_product_group_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "role"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "client_payments", "clients"
  add_foreign_key "client_phones", "clients"
  add_foreign_key "comandas", "clients"
  add_foreign_key "comandas", "dining_tables"
  add_foreign_key "down_payments", "employees"
  add_foreign_key "entrances", "providers"
  add_foreign_key "extras", "flavors"
  add_foreign_key "extras", "product_sizes"
  add_foreign_key "flavor_orders", "flavor_sizes"
  add_foreign_key "flavor_orders", "product_orders"
  add_foreign_key "flavor_sizes", "flavors"
  add_foreign_key "flavor_sizes", "product_sizes"
  add_foreign_key "flavors", "products"
  add_foreign_key "ingredient_entraces", "entrances"
  add_foreign_key "ingredient_entraces", "ingredients"
  add_foreign_key "ingredient_flavors", "flavors"
  add_foreign_key "ingredient_flavors", "ingredients"
  add_foreign_key "ingredients", "measures"
  add_foreign_key "order_payments", "clients"
  add_foreign_key "order_payments", "comandas"
  add_foreign_key "order_payments", "payment_types"
  add_foreign_key "orders", "cancel_reasons"
  add_foreign_key "orders", "comandas"
  add_foreign_key "orders", "users"
  add_foreign_key "pedido_extras", "extras"
  add_foreign_key "pedido_extras", "product_orders"
  add_foreign_key "product_orders", "cancel_reasons"
  add_foreign_key "product_orders", "flavor_sizes"
  add_foreign_key "product_orders", "orders"
  add_foreign_key "product_orders", "users"
  add_foreign_key "product_sizes", "products"
  add_foreign_key "products", "areas"
  add_foreign_key "products", "subgroups"
  add_foreign_key "promotion_flavor_sizes", "flavor_sizes"
  add_foreign_key "promotion_flavor_sizes", "promotions"
  add_foreign_key "promotion_groups", "promotions"
  add_foreign_key "promotion_groups", "subgroups"
  add_foreign_key "subgroups", "product_groups"
end
