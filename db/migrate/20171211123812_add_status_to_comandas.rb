class AddStatusToComandas < ActiveRecord::Migration[5.1]
  def change
    add_column :comandas, :status, :boolean
  end
end
