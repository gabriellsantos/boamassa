class AddMeasureToIngredients < ActiveRecord::Migration[5.1]
  def change
    add_reference :ingredients, :measure, foreign_key: true
  end
end
