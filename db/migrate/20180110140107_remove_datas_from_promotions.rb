class RemoveDatasFromPromotions < ActiveRecord::Migration[5.1]
  def change
    remove_column :promotions, :start_at, :date
    remove_column :promotions, :end_at, :date
  end
end
