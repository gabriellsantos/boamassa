class AddFlavorSizeToProductOrders < ActiveRecord::Migration[5.1]
  def change
    add_reference :product_orders, :flavor_size, foreign_key: true
  end
end
