class AddPrinterToArea < ActiveRecord::Migration[5.1]
  def change
    add_column :areas, :printer, :string
  end
end
