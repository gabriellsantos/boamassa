class AddNumeroComplementoToProviders < ActiveRecord::Migration[5.1]
  def change
    add_column :providers, :numero, :string
    add_column :providers, :complemento, :string
  end
end
