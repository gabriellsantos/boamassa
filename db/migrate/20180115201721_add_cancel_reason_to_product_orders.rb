class AddCancelReasonToProductOrders < ActiveRecord::Migration[5.1]
  def change
    add_reference :product_orders, :cancel_reason, foreign_key: true
  end
end
