class AddTaghereTokenToClient < ActiveRecord::Migration[5.1]
  def change
    add_column :clients, :taghere_token, :string
  end
end
