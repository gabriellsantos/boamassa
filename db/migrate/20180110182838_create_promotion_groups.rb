class CreatePromotionGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :promotion_groups do |t|
      t.references :subgroup, foreign_key: true
      t.references :promotion, foreign_key: true
      t.decimal :discount

      t.timestamps
    end
  end
end
