class AddClientToComanda < ActiveRecord::Migration[5.1]
  def change
    add_reference :comandas, :client, foreign_key: true
  end
end
