class CreateFlavorOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :flavor_orders do |t|
      t.references :product_order, foreign_key: true
      t.references :flavor_size, foreign_key: true

      t.timestamps
    end
  end
end
