class AddVisibleToFlavorSizes < ActiveRecord::Migration[5.1]
  def change
    add_column :flavor_sizes, :visible, :boolean
  end
end
