class CreatePromotionFlavorSizes < ActiveRecord::Migration[5.1]
  def change
    create_table :promotion_flavor_sizes do |t|
      t.decimal :discount
      t.references :promotion, foreign_key: true
      t.references :flavor_size, foreign_key: true

      t.timestamps
    end
  end
end
