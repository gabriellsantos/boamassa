class CreateIngredientFlavors < ActiveRecord::Migration[5.1]
  def change
    create_table :ingredient_flavors do |t|
      t.references :ingredient, foreign_key: true
      t.references :flavor, foreign_key: true

      t.timestamps
    end
  end
end
