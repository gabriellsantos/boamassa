class AddNoteToDownPayment < ActiveRecord::Migration[5.1]
  def change
    add_column :down_payments, :note, :string
  end
end
