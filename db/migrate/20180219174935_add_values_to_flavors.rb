class AddValuesToFlavors < ActiveRecord::Migration[5.1]
  def change
    add_column :flavors, :value, :decimal
    add_column :flavors, :active, :boolean
    add_column :flavors, :visible, :boolean
  end
end
