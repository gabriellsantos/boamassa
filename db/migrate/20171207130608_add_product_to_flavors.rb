class AddProductToFlavors < ActiveRecord::Migration[5.1]
  def change
    add_reference :flavors, :product, foreign_key: true
  end
end
