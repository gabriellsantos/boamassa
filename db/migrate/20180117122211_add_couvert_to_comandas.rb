class AddCouvertToComandas < ActiveRecord::Migration[5.1]
  def change
    add_column :comandas, :couvert_value, :decimal
    add_column :comandas, :couvert_quantity, :integer
  end
end
