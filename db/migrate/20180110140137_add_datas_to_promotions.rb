class AddDatasToPromotions < ActiveRecord::Migration[5.1]
  def change
    add_column :promotions, :start_at, :datetime
    add_column :promotions, :end_at, :datetime
  end
end
