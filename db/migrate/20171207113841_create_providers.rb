class CreateProviders < ActiveRecord::Migration[5.1]
  def change
    create_table :providers do |t|
      t.string :name
      t.string :inscricao
      t.string :inscricao_estadual
      t.string :nome_fantasia
      t.string :cep
      t.string :endereco
      t.string :bairro
      t.string :cidade
      t.string :estado
      t.string :email
      t.string :observacoes
      t.string :telefones
      t.string :site
      t.boolean :ativo

      t.timestamps
    end
  end
end
