class AddFechadaToComanda < ActiveRecord::Migration[5.1]
  def change
    add_column :comandas, :fechada, :boolean
  end
end
