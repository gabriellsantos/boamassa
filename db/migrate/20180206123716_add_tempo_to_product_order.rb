class AddTempoToProductOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :product_orders, :tempo, :string
  end
end
