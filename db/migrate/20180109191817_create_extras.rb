class CreateExtras < ActiveRecord::Migration[5.1]
  def change
    create_table :extras do |t|
      t.references :product_size, foreign_key: true
      t.references :flavor, foreign_key: true
      t.decimal :value
      t.boolean :active

      t.timestamps
    end
  end
end
