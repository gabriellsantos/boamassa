class CreateFlavorSizes < ActiveRecord::Migration[5.1]
  def change
    create_table :flavor_sizes do |t|
      t.references :flavor, foreign_key: true
      t.references :product_size, foreign_key: true
      t.decimal :value
      t.boolean :active

      t.timestamps
    end
  end
end
