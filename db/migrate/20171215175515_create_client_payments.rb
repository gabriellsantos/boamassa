class CreateClientPayments < ActiveRecord::Migration[5.1]
  def change
    create_table :client_payments do |t|
      t.references :client, foreign_key: true
      t.decimal :value

      t.timestamps
    end
  end
end
