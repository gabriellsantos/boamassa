class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.references :comanda, foreign_key: true
      t.string :status
      t.time :delivery_time
      t.time :delivery_duration

      t.timestamps
    end
  end
end
