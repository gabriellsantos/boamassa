class CreateDownPayments < ActiveRecord::Migration[5.1]
  def change
    create_table :down_payments do |t|
      t.references :employee, foreign_key: true
      t.decimal :value
      t.date :data

      t.timestamps
    end
  end
end
