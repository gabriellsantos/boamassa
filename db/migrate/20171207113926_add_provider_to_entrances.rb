class AddProviderToEntrances < ActiveRecord::Migration[5.1]
  def change
    add_reference :entrances, :provider, foreign_key: true
  end
end
