class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.string :photo
      t.references :subgroup, foreign_key: true
      t.references :area, foreign_key: true
      t.string :calculo
      t.boolean :active

      t.timestamps
    end
  end
end
