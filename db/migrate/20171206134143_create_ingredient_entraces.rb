class CreateIngredientEntraces < ActiveRecord::Migration[5.1]
  def change
    create_table :ingredient_entraces do |t|
      t.references :entrance, foreign_key: true
      t.references :ingredient, foreign_key: true
      t.decimal :value

      t.timestamps
    end
  end
end
