class CreateProductOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :product_orders do |t|
      t.references :order, foreign_key: true
      t.integer :quantity
      t.string :note
      t.string :status
      t.decimal :value

      t.timestamps
    end
  end
end
