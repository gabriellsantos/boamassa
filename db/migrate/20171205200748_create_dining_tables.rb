class CreateDiningTables < ActiveRecord::Migration[5.1]
  def change
    create_table :dining_tables do |t|
      t.string :number
      t.string :beacon
      t.string :status

      t.timestamps
    end
  end
end
