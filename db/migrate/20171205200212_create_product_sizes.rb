class CreateProductSizes < ActiveRecord::Migration[5.1]
  def change
    create_table :product_sizes do |t|
      t.references :product, foreign_key: true
      t.string :name
      t.string :description
      t.integer :quantity_flavors
      t.decimal :value
      t.boolean :active

      t.timestamps
    end
  end
end
