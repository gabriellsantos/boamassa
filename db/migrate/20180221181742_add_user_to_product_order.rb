class AddUserToProductOrder < ActiveRecord::Migration[5.1]
  def change
    add_reference :product_orders, :user, foreign_key: true
  end
end
