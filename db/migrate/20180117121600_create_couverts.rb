class CreateCouverts < ActiveRecord::Migration[5.1]
  def change
    create_table :couverts do |t|
      t.string :name
      t.decimal :value

      t.timestamps
    end
  end
end
