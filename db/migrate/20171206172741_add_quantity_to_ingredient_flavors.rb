class AddQuantityToIngredientFlavors < ActiveRecord::Migration[5.1]
  def change
    add_column :ingredient_flavors, :quantity, :integer
  end
end
