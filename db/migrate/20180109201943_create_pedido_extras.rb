class CreatePedidoExtras < ActiveRecord::Migration[5.1]
  def change
    create_table :pedido_extras do |t|
      t.references :product_order, foreign_key: true
      t.references :extra, foreign_key: true
      t.decimal :value

      t.timestamps
    end
  end
end
