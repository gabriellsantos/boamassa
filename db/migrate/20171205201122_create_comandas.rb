class CreateComandas < ActiveRecord::Migration[5.1]
  def change
    create_table :comandas do |t|
      t.references :dining_table, foreign_key: true
      t.string :tipo
      t.boolean :entrega

      t.timestamps
    end
  end
end
