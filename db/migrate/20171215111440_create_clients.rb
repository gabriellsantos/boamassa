class CreateClients < ActiveRecord::Migration[5.1]
  def change
    create_table :clients do |t|
      t.string :nome
      t.string :cpf_cnpj
      t.string :inscricao_estadual
      t.string :nome_fantasia
      t.string :cep
      t.string :endereco
      t.string :bairro
      t.string :complemento
      t.string :cidade
      t.string :estado
      t.string :email
      t.string :observacoes

      t.timestamps
    end
  end
end
