class AddComandaToOrderPayments < ActiveRecord::Migration[5.1]
  def change
    add_reference :order_payments, :comanda, foreign_key: true
  end
end
