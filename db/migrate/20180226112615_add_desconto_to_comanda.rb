class AddDescontoToComanda < ActiveRecord::Migration[5.1]
  def change
    add_column :comandas, :desconto, :decimal
  end
end
