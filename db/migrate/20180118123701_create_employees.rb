class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :name
      t.string :address
      t.string :pis
      t.string :phone
      t.string :cpf
      t.string :rg
      t.date :hiring_date
      t.date :demission_date

      t.timestamps
    end
  end
end
