class CreateOrderPayments < ActiveRecord::Migration[5.1]
  def change
    create_table :order_payments do |t|
      t.references :payment_type, foreign_key: true
      t.decimal :value
      t.references :client, foreign_key: true
      t.boolean :paid

      t.timestamps
    end
  end
end
