class AddContaazulIdToClient < ActiveRecord::Migration[5.1]
  def change
    add_column :clients, :contaazul_id, :string
  end
end
