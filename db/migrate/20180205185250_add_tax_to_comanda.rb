class AddTaxToComanda < ActiveRecord::Migration[5.1]
  def change
    add_column :comandas, :tax, :decimal
  end
end
