class AddTaghereUidToClient < ActiveRecord::Migration[5.1]
  def change
    add_column :clients, :taghere_uid, :string
  end
end
