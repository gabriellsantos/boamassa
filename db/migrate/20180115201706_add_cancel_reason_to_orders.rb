class AddCancelReasonToOrders < ActiveRecord::Migration[5.1]
  def change
    add_reference :orders, :cancel_reason, foreign_key: true
  end
end
