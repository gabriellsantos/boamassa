Measure.create!([
  {name: "UNIDADE", sigla: "UND"},
  {name: "PACOTE", sigla: "PCT"},
  {name: "ROLO", sigla: "RL"},
  {name: "RESMA", sigla: "RSM"},
  {name: "FARDO", sigla: "FD"},
  {name: "METRO", sigla: "M"},
  {name: "JOGO", sigla: "JG"},
  {name: "GALÃO", sigla: "GAL"},
  {name: "CAIXA", sigla: "CX"},
  {name: "LITRO", sigla: "L"},
  {name: "BLOCO", sigla: "BL"},
  {name: "PAR", sigla: "PR"},
  {name: "QUILOGRAMA", sigla: "KG"},
  {name: "FRASCO", sigla: "FR"},
  {name: "PEÇA", sigla: "PÇ"},
  {name: "SACO", sigla: "SC"},
  {name: "FOLHAS", sigla: "FL"},
  {name: "DÚZIA", sigla: "DZ"},
  {name: "CARTELA", sigla: "CTL"},
  {name: "MAÇO", sigla: "MC"},
  {name: "MILHEIRO", sigla: "MIL"},
  {name: "CONJUNTO", sigla: "CJT"},
  {name: "TONELADA", sigla: "T"},
  {name: "LATA", sigla: "LT"},
  {name: "BARRA", sigla: "BR"},
  {name: "GRAMA", sigla: "G"},
  {name: "CENTIMETRO", sigla: "CM"},
  {name: "METRO QUADRADO", sigla: "M²"},
  {name: "METRO CÚBICO", sigla: "M³"},
  {name: "CENTIMETRO CÚBICO", sigla: "CM³"},
  {name: "MILILITROS", sigla: "mL"},
  {name: "GROSA", sigla: "GR"},
  {name: "CENTIMETRO QUADRADO", sigla: "CM²"}
])
