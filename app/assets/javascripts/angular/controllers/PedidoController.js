app.controller('pedidoController',function($scope,$http,$sce, hotkeys)
{
   $scope.produtos = [];
   $scope.categorias = [];
   $scope.subcategorias = [];
   $scope.filtrar = "";
   $scope.categoria_selecionada = "";
   $scope.subcategoria_selecionada = "";
   $scope.lista = [];
   $scope.produto_selecionado = "";
   $scope.sabores = [];
   $scope.comanda = "";
   $scope.access_token = null;
   $scope.adicionais = [];
    $scope.adicionais_escolhidos = [];


    $http.get("/product_groups.json").then(function success(data)
    {
        $scope.categorias = data.data;
    });

    $scope.change_categoria = function(sub)
    {
        $scope.subcategorias = sub.subgroups;
    };

    $scope.busca_produtos = function()
    {
        if($scope.subcategoria_selecionada)
        {
            $http.get("/flavor_sizes/produtos_por_categoria?subgroup="+$scope.subcategoria_selecionada.id).then(function success(data)
            {
                $scope.produtos = data.data;
            });
        }
    }

    $scope.insere_produto = function(produto)
    {
        var exist = false;

        //procura produto para atualizar quantidade
        if(produto.tamanho.quantity_flavors <= 0)
        {
            for(var i in $scope.lista)
            {
                if($scope.lista[i].id == produto.id)
                {
                    $scope.lista[i].quantity += 1 ;
                    exist = true;
                }
            }
        }
        //se o produto não existir na lista, insere!
        if(exist == false)
        {

            var prod = Object.assign({},produto);
            prod["quantity"] = 1;
            prod["note"] = "";

            $scope.lista.push(prod);
        }

        $scope.produto_selecionado = "";
        setTimeout(function(){
            console.log('foi');
            window.scrollTo(0,document.body.scrollHeight);
        },1000);
    }

    $scope.selecionar_sabor = function(produto)
    {
        $scope.produto_selecionado = produto;
        $scope.produto_selecionado["flavor_orders_attributes"] = [];
        $scope.produto_selecionado["pedido_extras_attributes"] = [];
        for(var i = 0; i < produto.tamanho.quantity_flavors;i++)
        {
            $scope.produto_selecionado.flavor_orders_attributes.push({});
        }
        $http.get("/flavor_sizes/retorna_adicionais?product="+produto.tamanho.id).then(function success(data)
        {
            $scope.adicionais = data.data;
        });
        $http.get("/flavor_sizes/retorna_sabores?product="+produto.tamanho.id).then(function success(data)
        {
            $scope.sabores = data.data;
            for(var i in $scope.sabores)
            {
                delete $scope.sabores[i].tamanho;
                delete $scope.sabores[i].produto;
            }
            openModal();
        });

    }

    $scope.imprime_sabor = function(sabor)
    {
        for(var i in $scope.sabores)
        {
            if($scope.sabores[i].id == sabor)
            {
                return $scope.sabores[i].sabor.name;
            }
        }
    }

    $scope.enviar_pedido = function()
    {
        swal("Enviando...");

        if($scope.lista.length > 0)
        {
            var lista_limpa = Object.assign({},$scope.lista);
            for(var i in lista_limpa)
            {
                lista_limpa[i]["flavor_size_id"] = lista_limpa[i].id;
                delete lista_limpa[i].id;
                delete lista_limpa[i].tamanho;
                delete lista_limpa[i].sabor;
                delete lista_limpa[i].produto;
                for(var b in lista_limpa[i].flavor_orders_attributes)
                {

                    if(lista_limpa[i].flavor_orders_attributes[b][b] != null)
                    {
                    lista_limpa[i].flavor_orders_attributes[b]["flavor_size_id"] = lista_limpa[i].flavor_orders_attributes[b][b].id;
                    delete lista_limpa[i].flavor_orders_attributes[b].id;
                    delete lista_limpa[i].flavor_orders_attributes[b].sabor;
                    delete lista_limpa[i].flavor_orders_attributes[b][b];
                    }
                    else
                    {
                        delete lista_limpa[i].flavor_orders_attributes[b][b];
                    }
                }
                for(var b in lista_limpa[i].pedido_extras_attributes)
                {
                    lista_limpa[i].pedido_extras_attributes[b]["extra_id"] = lista_limpa[i].pedido_extras_attributes[b].id;
                    delete lista_limpa[i].pedido_extras_attributes[b].id;
                }
            }
            //console.log("adicionais - "+JSON.stringify(lista_limpa));
            var order = {order:{comanda_id: $scope.comanda}, access_token: $scope.access_token };
            order["order"]["product_orders_attributes"] = lista_limpa;
            //console.log(JSON.stringify(order))
            var req = {method: "POST",url:"/orders.json",data:order};
            $http(req).then(function success(data)
            {
                if(data.data.msg == "saved")
                {
                    if($scope.access_token == null){
                        window.location = data.data.path;
                    } else{
                        window.location = "/taghere/success";
                    }
                }
                swal.close();
                console.log("msg - "+data.data.msg);
            });

        }
    }
    
    $scope.removeItem = function (index) {
        $scope.lista.splice(index, 1);

    }
});