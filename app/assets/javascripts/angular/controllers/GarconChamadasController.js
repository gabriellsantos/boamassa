app.controller('GarconChamadasController',function($scope, webNotification)
{
    $scope.requests = [];
    $scope.db = firebase.firestore();
    $scope.query = $scope.db.collection("requests").where("app", "==", "c07a97c1-852a-4879-835d-8558c5116433")
        .where("category", "==", "garçon")
        .where("status", "==", "Solicitado")
        .orderBy("created_at");


    $scope.query.onSnapshot(function(querySnapshot) {
            var list = [];
            querySnapshot.forEach(function(doc) {
                list.push({data: doc.data(), id: doc.id});
            });
            console.log("Current list: ", list);
            querySnapshot.docChanges.forEach(function(change) {
                if (change.type === "added") {
                    webNotification.showNotification('Solicitação', {body: 'Garçon sendo chamado'});
                }
            });
            $scope.requests = list;
            $scope.$apply();
        });

    $scope.atender = function(id){
        $scope.db.collection("requests").doc(id).update({status:"Atendido"})
        alert('Atendido')
    }
});

//641341089558-k9jkhk48j85asim1kfje5suulq2sf910.apps.googleusercontent.com
//dUjfcvA3jpx3oLQUxRH4L5pY