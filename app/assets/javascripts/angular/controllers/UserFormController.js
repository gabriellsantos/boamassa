app.controller('UserFormController',function($scope,$http,$sce, hotkeys)
{
    $scope.cpf = "";
    $scope.name = "";
    $scope.new_user= true;
    $scope.person = {};
    $scope.cpf_error = false;
    $scope.quering = true;

    $scope.check_cpf = function()
    {
        $http.get("/users/check_cpf",{
            params:{cpf: $scope.cpf}
        }).then(function success(data)
        {
            $scope.person = data.data.user;
            $scope.name = $scope.person.name;
            if(data.data.type == "user")
            {
                $scope.new_user = false;
                swal("Cadastro Existente", "Usuário já cadastrado na base", "info");
            }else
            {
                $("#user_name").focus();
            }
            $scope.cpf_error = !data.data.status;
            $scope.quering = !data.data.status;
        });
    };
});