app.controller('clientController',function($scope,$http,$sce, hotkeys)
{
    $scope.cpf = "";
    $scope.new_user= true;
    $scope.person = {};
    $scope.cpf_error = false;
    $scope.quering = true;
    $scope.cep = "";
    $scope.endereco = "";
    $scope.complemento = "";
    $scope.bairro = "";
    $scope.nome = "";
    $scope.alias = "";
    $scope.cidade = "";
    $scope.estado = "";

    $scope.check_cpf = function() {
        $http.get("/clients/check_cpf",{
            params:{inscricao: $scope.cpf}
        }).then(function success(data) {
            $scope.person = data.data.person;
            $scope.nome = $scope.person.name;
            $scope.alias = $scope.person.alias;
            $scope.cidade = $scope.person.city;
            $scope.estado = $scope.person.state;
            $scope.cep = $scope.person.zip_code;
            $scope.endereco = $scope.person.address;
            $scope.complemento = $scope.person.address_2;
            $scope.bairro = $scope.person.neighborhood;
            if(data.data.type == "user"){
                $scope.new_user = false
                $("#person_new").html("")
                swal("Cadastro Existente", "Pessoa já cadastrada na base, informe os contatos em observações", "info")
            }
            $scope.cpf_error = !data.data.status
            $scope.quering = !data.data.status
        });
    };

    //$scope.check_cpf();

    $scope.busca_endereco = function()
    {
        $http.get("http://viacep.com.br/ws/"+$scope.cep+"/json/").then(function success(data)
        {

            if(data.statusText == "OK")
            {

                if(data.data.hasOwnProperty("logradouro"))
                {
                    $scope.endereco = data.data.logradouro;
                    $scope.complemento = data.data.complemento;
                    $scope.bairro = data.data.bairro;
                    $scope.cidade = data.data.localidade;
                    $scope.estado = data.data.uf;
                    $("#client_endereco").focusin();
                    $("#client_complemento").focusin();
                    $("#client_bairro").focusin();
                    $("#client_cidade").focusin();
                    $("#client_estado").focusin();
                    $("#provider_endereco").focusin();
                    $("#provider_complemento").focusin();
                    $("#provider_bairro").focusin();
                    $("#provider_cidade").focusin();
                    $("#provider_estado").focusin();
                }

            }

        },function error(){

        });
    }
});