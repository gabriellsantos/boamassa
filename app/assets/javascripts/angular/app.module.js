var app = angular.module('app',
    ['ngAnimate','angular.filter','ngSanitize',
        'angular.viacep', 'cfp.hotkeys',
        'angular-web-notification']).run(function($rootScope, $location) {
    $rootScope.location = $location;
});

$(document).on('ready page:load', function(arguments) {
    //angular.bootstrap(document.body, ['app']);
});