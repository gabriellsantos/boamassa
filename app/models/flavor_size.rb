# == Schema Information
#
# Table name: flavor_sizes
#
#  id              :integer          not null, primary key
#  flavor_id       :integer
#  product_size_id :integer
#  value           :decimal(, )
#  active          :boolean
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  visible         :boolean
#
# Indexes
#
#  index_flavor_sizes_on_flavor_id        (flavor_id)
#  index_flavor_sizes_on_product_size_id  (product_size_id)
#
# Foreign Keys
#
#  fk_rails_...  (flavor_id => flavors.id)
#  fk_rails_...  (product_size_id => product_sizes.id)
#

class FlavorSize < ApplicationRecord
  has_paper_trail
  belongs_to :flavor, optional: true
  belongs_to :product_size
  has_one :product, through: :product_size
  has_many :promotion_flavor_sizes
  has_many :product_orders
  has_many :ingredient_flavors, through: :flavor

  accepts_nested_attributes_for :flavor, allow_destroy: true



  def to_s
    "#{self.flavor} - #{self.product_size}"
  end

  def valor_do_dia
    promocao = PromotionFlavorSize.ativo(DateTime.now, self.id)
    if promocao
      self.value - ( (self.value * promocao.discount)/100 )
    else
      self.value
    end

  end

  def as_json(options = {})
    s = super(options)
    s[:sabor] = self.flavor
    s[:tamanho] = self.product_size
    s[:produto] = self.product
    s[:valor_do_dia] = self.valor_do_dia
    s
  end
end
