# == Schema Information
#
# Table name: ingredients
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  quantity    :decimal(, )
#  measure_id  :integer
#
# Indexes
#
#  index_ingredients_on_measure_id  (measure_id)
#
# Foreign Keys
#
#  fk_rails_...  (measure_id => measures.id)
#

class Ingredient < ApplicationRecord
  has_paper_trail
  has_many :ingredient_entraces
  belongs_to :measure

  def to_s
    self.name
  end
end
