# == Schema Information
#
# Table name: configurations
#
#  id         :integer          not null, primary key
#  key        :string
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class AppConfiguration < ApplicationRecord
  self.table_name = "configurations"
end
