# == Schema Information
#
# Table name: promotion_flavor_sizes
#
#  id             :integer          not null, primary key
#  discount       :decimal(, )
#  promotion_id   :integer
#  flavor_size_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_promotion_flavor_sizes_on_flavor_size_id  (flavor_size_id)
#  index_promotion_flavor_sizes_on_promotion_id    (promotion_id)
#
# Foreign Keys
#
#  fk_rails_...  (flavor_size_id => flavor_sizes.id)
#  fk_rails_...  (promotion_id => promotions.id)
#

class PromotionFlavorSize < ApplicationRecord
  has_paper_trail
  belongs_to :promotion
  belongs_to :flavor_size
  #scope :ativo, lambda {|hoje| joins(:promotion).where("? between promotions.start_at  AND promotions.end_at ", hoje).first}

  def self.ativo(hoje,id)
    joins(:promotion).where("flavor_size_id = ? and ? between promotions.start_at  AND promotions.end_at ", id,hoje).first
  end
end
