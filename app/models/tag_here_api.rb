class TagHereApi
  def self.get_firebase_token
    JSON.parse(RestClient.get("http://citifystore.herokuapp.com/api/get_token?access_token=8dd8094673971e35f8438223af30cc91504ede7267dd3c134646801008aa3df9&app=1"))["token"]
  end

  def self.url
    "http://developers.taghere.com.br"
  end

  def self.call(method , endereco, params={})
    if method == :get
      params_url  = URI.unescape(params.to_param)
      endereco = url + endereco +"?"+ params_url
      call = RestClient.send(method, endereco)
    else
      endereco = url + endereco
      call = RestClient.send(method, endereco, params)
    end
    return JSON.parse(call, symbolize_names: true)
  end

  def self.get_user(access_token)
    call(:get,"/api/user.json", {access_token: access_token})
  end
end