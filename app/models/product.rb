# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  photo       :string
#  subgroup_id :integer
#  area_id     :integer
#  calculo     :string
#  active      :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_products_on_area_id      (area_id)
#  index_products_on_subgroup_id  (subgroup_id)
#
# Foreign Keys
#
#  fk_rails_...  (area_id => areas.id)
#  fk_rails_...  (subgroup_id => subgroups.id)
#

class Product < ApplicationRecord
  has_paper_trail
  belongs_to :subgroup, optional: true
  belongs_to :area, optional: true

  has_many :product_sizes


  has_many :flavor_sizes, through: :product_sizes
  has_many :flavors, through: :product_sizes
  has_many :extras, through: :product_sizes
  has_many :product_orders, through: :flavor_sizes


  has_many :ingredient_flavors, through: :flavors

  accepts_nested_attributes_for :product_sizes, allow_destroy: true
  accepts_nested_attributes_for :flavor_sizes, allow_destroy: true
  accepts_nested_attributes_for :extras, allow_destroy: true



  mount_uploader :photo, AvatarUploader

  extend Enumerize

  enumerize :calculo, in: [:unidade, :media, :maior_valor], predicates: true

  def to_s
    self.name
  end


end
