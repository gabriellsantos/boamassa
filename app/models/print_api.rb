class PrintApi < ApplicationRecord

  def self.client_secret
    "TGPZKqqQEbMGOEnkGVm2rkGZ"
  end

  def self.client_id
    "272457111549-7fqdgdle9ai1ofbl15kvqd7d8loj9s22.apps.googleusercontent.com"
  end

  def self.redirect_uri
    if Rails.env.production?
      "https://boamassa.herokuapp.com/google/callback"
    else
      "http://localhost:3000/google/callback"
    end
  end

  def self.client(redirect=false)
    options = {
        client_id: client_id,
        client_secret: client_secret,
    }
    if !redirect
      options[:refresh_token] = Configuration.find_by_key("google_refresh").value
    end
    CloudPrint::Client.new(options)
  end

  def self.generate_token(code)
    config = Configuration.find_or_create_by(key: "google_refresh")
    token = client.auth.generate_token(code, redirect_uri)
    config.update(value: token)
  end

  def self.printers
    PrintApi.client.printers.all
  end

  def self.printers_to_select
    #printers.map{|a| ["#{a.name} (#{a.connection_status})", a.id]}
    client_a.printers.map{|a| ["#{a.name}", a.id]}
  end

  def self.api_key
    "02d9cd9ef1de72df5119ed012d1451fc59d191db"
  end

  def  self.teste
    auth = PrintNode::Auth.new(api_key)
    client_a = PrintNode::Client.new(auth)


    pj = PrintNode::PrintJob.new("352898", "teste", "raw_uri", "http://boamassa.herokuapp.com/teste.txt", "teste")

    client_a.create_printjob(pj)
  end

  def self.client_a
    auth = PrintNode::Auth.new(api_key)
    client_a = PrintNode::Client.new(auth)
  end

  def self.send_print(url, printer)
    pj = PrintNode::PrintJob.new(printer, "teste", "raw_uri", url, "teste")

    client_a.create_printjob(pj)
  end
end
