# == Schema Information
#
# Table name: measures
#
#  id         :integer          not null, primary key
#  name       :string
#  sigla      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Measure < ApplicationRecord
  has_paper_trail
  has_many :ingredients

  def to_s
    self.sigla
  end
end
