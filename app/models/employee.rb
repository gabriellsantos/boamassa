# == Schema Information
#
# Table name: employees
#
#  id             :integer          not null, primary key
#  name           :string
#  address        :string
#  pis            :string
#  phone          :string
#  cpf            :string
#  rg             :string
#  hiring_date    :date
#  demission_date :date
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Employee < ApplicationRecord
  has_paper_trail
  has_many :down_payments

  accepts_nested_attributes_for :down_payments, allow_destroy: true

  validates :name, :address, :cpf, :phone, presence: true

  def to_s
    self.name
  end
end
