# == Schema Information
#
# Table name: dining_tables
#
#  id         :integer          not null, primary key
#  number     :string
#  beacon     :string
#  status     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class DiningTable < ApplicationRecord
  has_paper_trail
  has_many :comandas
  def to_s
    self.number
  end
end
