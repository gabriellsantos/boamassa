# == Schema Information
#
# Table name: promotion_groups
#
#  id           :integer          not null, primary key
#  subgroup_id  :integer
#  promotion_id :integer
#  discount     :decimal(, )
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_promotion_groups_on_promotion_id  (promotion_id)
#  index_promotion_groups_on_subgroup_id   (subgroup_id)
#
# Foreign Keys
#
#  fk_rails_...  (promotion_id => promotions.id)
#  fk_rails_...  (subgroup_id => subgroups.id)
#

class PromotionGroup < ApplicationRecord
  has_paper_trail
  belongs_to :subgroup
  belongs_to :promotion
  has_many :products, through: :subgroup
  has_many :flavor_sizes, through: :products
  
  after_save :insere_produtos
  after_destroy :remove_produtos

  def insere_produtos
    #remove os produtos do grupo e insere novamente
    PromotionFlavorSize.where(flavor_size_id: self.flavor_sizes).destroy_all
    self.flavor_sizes.each do |f|
      PromotionFlavorSize.create(discount: self.discount,promotion_id: self.promotion_id,flavor_size_id: f.id )
    end
  end

  def remove_produtos
    PromotionFlavorSize.where(flavor_size_id: self.flavor_sizes).destroy_all
  end
end
