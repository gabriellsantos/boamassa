# == Schema Information
#
# Table name: product_orders
#
#  id               :integer          not null, primary key
#  order_id         :integer
#  note             :string
#  status           :string
#  value            :decimal(, )
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  flavor_size_id   :integer
#  quantity         :decimal(, )
#  cancel_reason_id :integer
#  tempo            :string
#  user_id          :integer
#
# Indexes
#
#  index_product_orders_on_cancel_reason_id  (cancel_reason_id)
#  index_product_orders_on_flavor_size_id    (flavor_size_id)
#  index_product_orders_on_order_id          (order_id)
#  index_product_orders_on_user_id           (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (cancel_reason_id => cancel_reasons.id)
#  fk_rails_...  (flavor_size_id => flavor_sizes.id)
#  fk_rails_...  (order_id => orders.id)
#  fk_rails_...  (user_id => users.id)
#

class ProductOrder < ApplicationRecord
  has_paper_trail
  belongs_to :order
  belongs_to :cancel_reason, optional: true
  has_many :flavor_orders, dependent: :destroy
  has_many :pedido_extras, dependent: :destroy
  belongs_to :flavor_size
  belongs_to :user, optional: true
  has_one :product_size, through: :flavor_size
  has_one :product, through: :product_size
  has_one :comanda, through: :order
  has_one :dining_table, through: :comanda

  accepts_nested_attributes_for :flavor_orders,reject_if: proc {|attribute| attribute["flavor_size_id"].blank?}, allow_destroy: true
  accepts_nested_attributes_for :pedido_extras, allow_destroy: true

  extend Enumerize

  enumerize :status, in: [:fila, :em_producao,:pronto, :na_mesa, :concluido, :cancelado], predicates: true, default: :fila

  #before_save
  after_save :update_ingredients
  before_create :set_user,:calcula_valor

  def set_user
    if !self.user.present?
      self.user = self.order.user
    end

  end

  def calcula_valor
    if self.flavor_size.present?
      if self.flavor_size.product_size.product.calculo == :unidade
        self.value = self.flavor_size.valor_do_dia * self.quantity

      elsif self.flavor_size.product_size.product.calculo == :media

        if self.flavor_orders.present?
          soma = self.flavor_orders.map{|a| a.flavor_size.valor_do_dia}.sum
          self.value = (soma / (self.flavor_orders.size == 0 ? 1 : self.flavor_orders.size) ) * self.quantity
        else
          self.value = self.quantity * self.flavor_size.valor_do_dia
        end


      else

        self.value = self.flavor_orders.map{|a| a.flavor_size.valor_do_dia}.max
      end
    end
    if self.pedido_extras.present?
      self.value += self.pedido_extras.sum(:value)
    end

  end


  def update_ingredients
    if self.status == :concluido
      if flavor_orders.present?
        flavor_orders.each do |p|
          p.flavor_size.flavor.ingredient_flavors.each do |i|
            ingrediente = i.ingredient
            ingrediente.quantity -= (i.quantity * (self.quantity/self.flavor_orders.count) )
            ingrediente.save
          end
        end
      else
        flavor_size.flavor.ingredient_flavors.each do |i|
          ingrediente = i.ingredient
          ingrediente.quantity -= (i.quantity * self.quantity)
          ingrediente.save
        end
      end

    end
  end
end
