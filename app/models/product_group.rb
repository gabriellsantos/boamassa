# == Schema Information
#
# Table name: product_groups
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ProductGroup < ApplicationRecord
  has_paper_trail
  has_many :subgroups

  accepts_nested_attributes_for :subgroups, allow_destroy: true
end
