# == Schema Information
#
# Table name: product_sizes
#
#  id               :integer          not null, primary key
#  product_id       :integer
#  name             :string
#  description      :string
#  quantity_flavors :integer
#  value            :decimal(, )
#  active           :boolean
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_product_sizes_on_product_id  (product_id)
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#

class ProductSize < ApplicationRecord
  has_paper_trail
  belongs_to :product, optional: true

  has_many :flavor_sizes
  has_many :flavors, through: :flavor_sizes
  has_many :extras
  has_many :flavor_extras, class_name: "Flavor", through: :extras

  accepts_nested_attributes_for :flavor_sizes, allow_destroy: true
  accepts_nested_attributes_for :extras, allow_destroy: true
  accepts_nested_attributes_for :flavors, allow_destroy: true
  accepts_nested_attributes_for :flavor_extras, allow_destroy: true

  def to_s
    self.name
  end
end
