# == Schema Information
#
# Table name: orders
#
#  id                :integer          not null, primary key
#  comanda_id        :integer
#  status            :string
#  delivery_time     :time
#  delivery_duration :time
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  cancel_reason_id  :integer
#  user_id           :integer
#
# Indexes
#
#  index_orders_on_cancel_reason_id  (cancel_reason_id)
#  index_orders_on_comanda_id        (comanda_id)
#  index_orders_on_user_id           (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (cancel_reason_id => cancel_reasons.id)
#  fk_rails_...  (comanda_id => comandas.id)
#  fk_rails_...  (user_id => users.id)
#

class Order < ApplicationRecord
  has_paper_trail
  belongs_to :comanda
  belongs_to :cancel_reason, optional: true
  belongs_to :user, optional: true
  has_many :product_orders, dependent: :destroy

  accepts_nested_attributes_for :product_orders, allow_destroy: true

  extend Enumerize

  enumerize :status, in: [:fila, :em_producao, :na_mesa, :concluido, :cancelado], predicates: true, default: :fila

  after_save :cancelar_items, :set_tax
  after_create :print_cloud
  after_destroy :set_tax


  def cancelar_items
    if self.status == :cancelado
      self.product_orders.each do |p|
        p.status = :cancelado
        p.cancel_reason_id = self.cancel_reason_id
        p.save
      end
    end
  end

  def print_cloud_legacy
    begin
    product_orders.group_by{|a| a.flavor_size.product.area}.map do |area, po|
      if area.printer.present?
        template = ApplicationController.render(
            :template => 'orders/print',
            :layout => false,
            :assigns => {
                order: self,
                product_orders: po,
                area: area
            }
        )
        printer = PrintApi.client.printers.find(area.printer)
        printer.print(title: "Pedido #{self.id} - Mesa #{comanda.dining_table} - #{area}", content: template, content_type: "text/html")
      end
    end
    rescue
    end

  end

  def print_cloud
    if comanda.print?
      begin
      product_orders.group_by{|a| a.flavor_size.product.area}.map do |area, po|
        if area.printer.present? #and (self.comanda.tipo.caixa? and self.comanda.entrega )
          PrintApi.send_print(
              "http://boamassa.herokuapp.com/orders/#{po.last.order.id}/print.txt?area=#{area.id}",
              area.printer
          )
        end
      end
      rescue
      end
    end


  end

  def set_tax
    comanda.set_tax
    comanda.save
  end
end
