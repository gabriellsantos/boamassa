# == Schema Information
#
# Table name: cancel_reasons
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CancelReason < ApplicationRecord
  has_paper_trail
  has_many :orders
  has_many :product_orders
end
