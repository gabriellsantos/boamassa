class CnpjApi
  require "cpf_cnpj"
  def self.url(cnpj)
    if !Rails.env.production?
      return "https://api.cpfcnpj.com.br/5ae973d7a997af13f0aaf2bf60e65803/4/json/#{cnpj}"
    end
    "https://api.cpfcnpj.com.br/f54a04f5cd4031c6a0e5a28de8b588a0/4/json/#{cnpj}"
  end

  def self.consultar(cnpj)

    JSON.parse(RestClient.get(url(cnpj)), symbolize_names: true)
  end

  def initialize(cnpj)
    if CNPJ.valid?(cnpj)
      attributes = CnpjApi.consultar(cnpj)
    else
      attributes = {}
    end
    @name = attributes[:razao]
    @cnpj = cnpj
    @status = attributes[:status]
    @alias = attributes[:fantasia]
    @zip_code = attributes[:cep]
    @address= attributes[:endereco]
    @numero = attributes[:numero]
    @address_2 = attributes[:complemento]
    @city = attributes[:cidade]
    @neighborhood = attributes[:bairro]
    @state = attributes[:uf]
    @date = attributes[:inicioAtividade]
  end

  def cnpj
    @cnpj
  end

  def name
    @name
  end

  def status
    @status
  end

  def alias
    @alias
  end

  def zip_code
    @zip_code
  end

  def address
    @address
  end

  def numero
    @numero
  end

  def address_2
    @address_2
  end

  def city
    @city
  end

  def neighborhood
    @neighborhood
  end

  def state
    @state
  end

  def date
    @date
  end

  def secret
    CpfApi.json_crypt.encrypt_and_sign(self.to_json)
  end

  def self.json_crypt
    ActiveSupport::MessageEncryptor.new(Rails.application.secrets.secret_key_base)
  end

  def self.json_decrypt(code)
    JSON.parse(json_crypt.decrypt_and_verify(code), symbolize_names: true)
  end
end