# == Schema Information
#
# Table name: subgroups
#
#  id               :integer          not null, primary key
#  name             :string
#  product_group_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_subgroups_on_product_group_id  (product_group_id)
#
# Foreign Keys
#
#  fk_rails_...  (product_group_id => product_groups.id)
#

class Subgroup < ApplicationRecord
  has_paper_trail
  belongs_to :product_group
  has_many :products,dependent: :nullify

  def to_s
    self.name
  end
end
