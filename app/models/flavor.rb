# == Schema Information
#
# Table name: flavors
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  product_id  :integer
#  value       :decimal(, )
#  active      :boolean
#  visible     :boolean
#
# Indexes
#
#  index_flavors_on_product_id  (product_id)
#
# Foreign Keys
#
#  fk_rails_...  (product_id => products.id)
#

class Flavor < ApplicationRecord
  has_paper_trail
  has_many :ingredient_flavors
  has_one :extra
  has_one :flavor_size
  belongs_to :product, optional: true

  accepts_nested_attributes_for :ingredient_flavors, allow_destroy: true
  
  after_save :atualizar_flavor_size
  after_create :atualizar_produto

  def to_s
    self.name
  end

  def atualizar_produto
    if self.flavor_size.present?
      update(product: self.flavor_size.product_size.product)
    end
  end



  def atualizar_flavor_size
    if self.flavor_size.present?
      self.flavor_size.update(value: self.value, active: self.active, visible: self.visible)
    end
    if self.extra.present?
      self.extra.update(value: self.value, active: self.active)
    end

  end
end
