# == Schema Information
#
# Table name: areas
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  printer    :string
#

class Area < ApplicationRecord
  has_paper_trail
  has_many :products,dependent: :nullify
  has_many :product_orders, through: :products
  def to_s
    self.name
  end
end
