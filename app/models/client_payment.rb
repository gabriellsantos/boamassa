# == Schema Information
#
# Table name: client_payments
#
#  id         :integer          not null, primary key
#  client_id  :integer
#  value      :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_client_payments_on_client_id  (client_id)
#
# Foreign Keys
#
#  fk_rails_...  (client_id => clients.id)
#

class ClientPayment < ApplicationRecord
  has_paper_trail
  belongs_to :client
end
