class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.admin?
      can :manage, :all
    elsif user.garcom? || user.caixa?
      can [:home,:comandas], DiningTable
      can [:show,:novo_pedido,:pedido_simples, :create, :fechar,:inserir_couvert], Comanda
      can [:change_comanda,:cancelar,:create],Order
      can [:change_comanda,:cancelar,:concluir],ProductOrder
      can [:index], ProductGroup
      can [:produtos_por_categoria,:retorna_adicionais,:retorna_sabores], FlavorSize
    elsif user.caixa?
      can [:concluir,:calcular_desconto], Comanda
      can [:manage], Client


    elsif user.producao?
      can [:show,:index], Area
      can [:atualiza_pedido], ProductOrder
    elsif user.cliente?
    else
    end
    can [:index], ProductGroup
    can [:produtos_por_categoria,:retorna_adicionais,:retorna_sabores], FlavorSize
    can [:create], Order

  end
end
