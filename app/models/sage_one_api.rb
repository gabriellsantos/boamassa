class SageOneApi
  def self.url
    "https://app.sageone.com"
  end
  def self.url_br
    "https://api.sageone.com"
  end

  def self.redirect_url
    "https://localhost:3000"
  end

  def self.client
    OAuth2::Client.new(
        '5cb9a2251f13b3d40f7e', '9f2a48e2e65dc0d81b412506029192c6ed6b0e0c',
        site: self.url,
        token_url: "/oauth2/token"
    )
  end

  def self.get_token(code)
    client.auth_code.get_token(
        code,
        :redirect_uri => redirect_url,
        :headers => {'Authorization' => 'Basic some_password'}
    )
  end

  def self.refresh_token
    config= Configuration.find_by_key("sage_refresh")
    response = RestClient.post(
        "#{url}/oauth2/token",
        {
            client_id: '5cb9a2251f13b3d40f7e',
            client_secret: '9f2a48e2e65dc0d81b412506029192c6ed6b0e0c',
            grant_type: :refresh_token,
            refresh_token: config.value
        }
    )
    data = JSON.parse(response)
    config.update(value: data["refresh_token"])
    data["access_token"]
  end


  def self.call(method , endereco, params={})
    begin
      if method == :get
        params_url  = URI.unescape(params.to_param)
        endereco = url_br + endereco +"?"+ params_url
        sage = sigin(method, endereco, params)
        headers = sage.request_headers("Cantina Boamassa")
        call = RestClient.get(endereco, headers= headers)
      else
        endereco = url_br + endereco
        sage = sigin(method, endereco, params)
        headers = sage.request_headers("Cantina Boamassa")
        call = RestClient.send(method, endereco, params, headers = headers)
      end
    rescue RestClient::ExceptionWithResponse => e
      return {a: e.response, b: headers}
    end
    return JSON.parse(call, symbolize_names: true)
  end

  def self.sigin(method, endereco, params)
    SageoneApiSigner.new(
        {
            request_method: 'get',
            url: 'https://api.sageone.com/test/accounts/v1/contacts',
            body_params: {},
            signing_secret: "d6c18810494be6a98734b0cc7dc8b1033c7ccc4b",
            access_token: refresh_token,
        }
    )
  end

end