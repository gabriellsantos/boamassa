# == Schema Information
#
# Table name: entrances
#
#  id          :integer          not null, primary key
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  provider_id :integer
#
# Indexes
#
#  index_entrances_on_provider_id  (provider_id)
#
# Foreign Keys
#
#  fk_rails_...  (provider_id => providers.id)
#

class Entrance < ApplicationRecord
  has_paper_trail
  has_many :ingredient_entraces
  belongs_to :provider

  accepts_nested_attributes_for :ingredient_entraces, allow_destroy: true
  
  after_create :atualizar_estoque

  #atualiza o estoque dos ingredientes
  def atualizar_estoque
    self.ingredient_entraces.each do |i|
      p = Ingredient.find(i.ingredient_id)
      p.quantity += i.quantity
      p.save
    end
  end
end
