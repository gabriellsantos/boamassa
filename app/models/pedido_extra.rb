# == Schema Information
#
# Table name: pedido_extras
#
#  id               :integer          not null, primary key
#  product_order_id :integer
#  extra_id         :integer
#  value            :decimal(, )
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_pedido_extras_on_extra_id          (extra_id)
#  index_pedido_extras_on_product_order_id  (product_order_id)
#
# Foreign Keys
#
#  fk_rails_...  (extra_id => extras.id)
#  fk_rails_...  (product_order_id => product_orders.id)
#

class PedidoExtra < ApplicationRecord
  has_paper_trail
  belongs_to :product_order
  belongs_to :extra

  after_save :recalcula_valor_produto

  def recalcula_valor_produto
    produto = self.product_order
    produto.value += self.value
    produto.save
  end
end
