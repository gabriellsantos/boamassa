# == Schema Information
#
# Table name: extras
#
#  id              :integer          not null, primary key
#  product_size_id :integer
#  flavor_id       :integer
#  value           :decimal(, )
#  active          :boolean
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_extras_on_flavor_id        (flavor_id)
#  index_extras_on_product_size_id  (product_size_id)
#
# Foreign Keys
#
#  fk_rails_...  (flavor_id => flavors.id)
#  fk_rails_...  (product_size_id => product_sizes.id)
#

class Extra < ApplicationRecord
  has_paper_trail
  belongs_to :product_size
  belongs_to :flavor_extra, class_name: "Flavor", foreign_key: :flavor_id

  def as_json(options = {})
    s = super(options)
    s[:sabor] = self.flavor_extra
    s
  end

  after_create :atualizar_produto

  def atualizar_produto
    self.flavor_extra.update(product: self.product_size.product)
    update(value: self.flavor_extra.value, active: self.flavor_extra.active)
  end

end
