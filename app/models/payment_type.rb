# == Schema Informationcrédito
#
# Table name: payment_types
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class PaymentType < ApplicationRecord
  has_paper_trail
  has_many :order_payments

  def to_s
    self.name
  end
end
