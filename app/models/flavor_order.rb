# == Schema Information
#
# Table name: flavor_orders
#
#  id               :integer          not null, primary key
#  product_order_id :integer
#  flavor_size_id   :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_flavor_orders_on_flavor_size_id    (flavor_size_id)
#  index_flavor_orders_on_product_order_id  (product_order_id)
#
# Foreign Keys
#
#  fk_rails_...  (flavor_size_id => flavor_sizes.id)
#  fk_rails_...  (product_order_id => product_orders.id)
#

class FlavorOrder < ApplicationRecord
  has_paper_trail
  belongs_to :product_order
  belongs_to :flavor_size
  has_one :product_size, through: :flavor_size
end
