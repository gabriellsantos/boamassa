# == Schema Information
#
# Table name: promotions
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  start_at   :datetime
#  end_at     :datetime
#

class Promotion < ApplicationRecord
  has_paper_trail
  has_many :promotion_flavor_sizes, dependent: :destroy
  has_many :promotion_groups, dependent: :destroy

  accepts_nested_attributes_for :promotion_flavor_sizes, allow_destroy: true
  accepts_nested_attributes_for :promotion_groups, allow_destroy: true
end
