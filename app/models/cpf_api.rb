class CpfApi
  require "cpf_cnpj"
  def self.url(cpf)
    if !Rails.env.production?
      return "https://api.cpfcnpj.com.br/5ae973d7a997af13f0aaf2bf60e65803/1/json/#{cpf}"
    end
    "https://api.cpfcnpj.com.br/f54a04f5cd4031c6a0e5a28de8b588a0/1/json/#{cpf}"
  end

  def self.consultar(cpf)
    
    JSON.parse(RestClient.get(url(cpf)), symbolize_names: true)
  end

  def initialize(cpf)
    if CPF.valid?(cpf)
      attributes = CpfApi.consultar(cpf)
    else
      attributes = {}
    end
    @name = attributes[:nome]
    @cpf = cpf
    @status = attributes[:status]
    @sex = attributes[:genero]
    @birth = attributes[:nascimento]
    @balance= attributes[:saldo]
    @mother = attributes[:mae]
    @search_id = attributes[:ConsultaID]
    @delay = attributes[:delay]
    @plan = attributes[:pacodeUsado]
  end

  def cpf
    @cpf
  end

  def name
    @name
  end

  def status
    @status
  end

  def sex
    @sex
  end

  def birth
    @birth
  end

  def balance
    @balance
  end

  def mother
    @mother
  end

  def search_id
    @search_id
  end

  def delay
    @delay
  end

  def plan
    @plan
  end

  def secret
    CpfApi.json_crypt.encrypt_and_sign(self.to_json)
  end

  def self.json_crypt
    ActiveSupport::MessageEncryptor.new(Rails.application.secrets.secret_key_base)
  end

  def self.json_decrypt(code)
    JSON.parse(json_crypt.decrypt_and_verify(code), symbolize_names: true)
  end
end