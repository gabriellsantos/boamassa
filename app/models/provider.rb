# == Schema Information
#
# Table name: providers
#
#  id                 :integer          not null, primary key
#  name               :string
#  inscricao          :string
#  inscricao_estadual :string
#  nome_fantasia      :string
#  cep                :string
#  endereco           :string
#  bairro             :string
#  cidade             :string
#  estado             :string
#  email              :string
#  observacoes        :string
#  telefones          :string
#  site               :string
#  ativo              :boolean
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  numero             :string
#  complemento        :string
#

class Provider < ApplicationRecord
  has_paper_trail
  has_many :entrances

  validates :name, :inscricao, :inscricao_estadual, :cep, :endereco,:telefones, presence: true

  def to_s
    self.name
  end

end
