# == Schema Information
#
# Table name: ingredient_entraces
#
#  id            :integer          not null, primary key
#  entrance_id   :integer
#  ingredient_id :integer
#  value         :decimal(, )
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  quantity      :integer
#
# Indexes
#
#  index_ingredient_entraces_on_entrance_id    (entrance_id)
#  index_ingredient_entraces_on_ingredient_id  (ingredient_id)
#
# Foreign Keys
#
#  fk_rails_...  (entrance_id => entrances.id)
#  fk_rails_...  (ingredient_id => ingredients.id)
#

class IngredientEntrace < ApplicationRecord
  has_paper_trail
  belongs_to :entrance
  belongs_to :ingredient
end
