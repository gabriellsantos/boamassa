# == Schema Information
#
# Table name: comandas
#
#  id               :integer          not null, primary key
#  dining_table_id  :integer
#  tipo             :string
#  entrega          :boolean
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  status           :boolean
#  client_id        :integer
#  couvert_value    :decimal(, )
#  couvert_quantity :integer
#  tax              :decimal(, )
#  fechada          :boolean
#  contaazul_id     :string
#  desconto         :decimal(, )
#  who_close        :integer
#
# Indexes
#
#  index_comandas_on_client_id        (client_id)
#  index_comandas_on_dining_table_id  (dining_table_id)
#
# Foreign Keys
#
#  fk_rails_...  (client_id => clients.id)
#  fk_rails_...  (dining_table_id => dining_tables.id)
#

class Comanda < ApplicationRecord
  has_paper_trail
  belongs_to :dining_table , optional: true
  belongs_to :client , optional: true
  has_many :orders, dependent: :destroy
  has_many :product_orders, through: :orders
  has_many :flavor_orders, through: :product_orders
  has_many :order_payments, dependent: :destroy
  has_many :flavor_sizes, through: :product_orders
  belongs_to :user, class_name: "User", foreign_key: :who_close,optional: true

  accepts_nested_attributes_for :orders, allow_destroy: true
  accepts_nested_attributes_for :product_orders, allow_destroy: true
  accepts_nested_attributes_for :flavor_orders, allow_destroy: true
  accepts_nested_attributes_for :order_payments, allow_destroy: true

  extend Enumerize

  enumerize :tipo, in: [:garcom, :app, :delivery,:caixa], predicates: true

  validate :check_payments
  before_validation :set_tax

  def sync_contaazul
    begin
      if self.contaazul_id.present?
        update_on_contaazul
      else
        create_on_contaazul
      end
    rescue
    end
  end

  def print_cloud
    if self.fechada
      begin
        area = Area.find_by_name("Caixa")
        if area.printer.present?
            PrintApi.send_print(
                "http://boamassa.herokuapp.com/comandas/#{self.id}/imprimir.txt",
                area.printer
            )
        end
      rescue
      end
    end

  end

  def update_on_contaazul
    params = contaazul_params
    response = ContaazulApi.call(:put, "/v1/sales/#{self.contaazul_id}", params)
  end

  def create_on_contaazul
    params = contaazul_params
    response = ContaazulApi.call(:post, "/v1/sales", params)
    self.contaazul_id = response[:id]
    response
  end

  def contaazul_params

    products = product_orders.where.not(status: :cancelado).map do |po|
      {
        "description": po.flavor_size.to_s,
        "quantity": po.quantity,
        "value": po.value,
        "product_id": "d4f1ace9-9d29-4cca-8219-71325679cbda"
      }
    end

    payments = order_payments.map do |op|
      {
      "number": op.id,
      "value": op.value,
      "due_date": DateTime.now,
      "status": "ACQUITTED"
      }
    end
    params = {
      "number": self.id,
      "status": "COMMITTED",
      "emission": DateTime.now,
      "payment": {
          "type": "CASH",
          "installments": payments
      },
      "services": [
        {
            "description": "Couvert",
            "quantity": self.couvert_quantity || 1,
            "value": self.couvert_value || 0,
            "service_id": "161ed420-d2f7-49d9-8955-4773e171dd16",
        },
        {
            "description": "Taxa de Serviço",
            "quantity": 1,
            "value": self.tax || 0,
            "service_id": "5254257a-569b-40a1-b719-7e92942c11b9",
        }
      ],
      "products": products
    }
    if client.present? and client.contaazul_id.present?
      params[:customer_id]= client.contaazul_id
    else
      params[:customer_id]= "83f16edc-5f9e-457f-868a-638f52febd8a"
    end
    params
  end
  def self.actives
    where(status: false)
  end

  def check_payments
    if self.status
      total_pago = self.order_payments.sum(&:value)
      total = self.product_orders.where.not(status: :cancelado).sum(&:value) + (self.tax.round(2) || 0) + couvert_calculado - (self.desconto || 0)
      if total_pago != total
        errors.add(:base,"Confira os pagamentos!")
      end
      errors.blank?
    end

  end

  def total_orders
    product_orders.where.not(status: :cancelado).sum(&:value)
  end

  def set_tax
    if garcom?
      self.tax = total_orders * 0.1
    end
  end

  def to_s
    self.id
  end

  def couvert_calculado
    (self.couvert_value || 0 ) * (self.couvert_quantity || 1)
  end

  def print?
    (caixa? and entrega) or !caixa?
  end


end
