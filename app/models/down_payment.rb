# == Schema Information
#
# Table name: down_payments
#
#  id          :integer          not null, primary key
#  employee_id :integer
#  value       :decimal(, )
#  data        :date
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  note        :string
#
# Indexes
#
#  index_down_payments_on_employee_id  (employee_id)
#
# Foreign Keys
#
#  fk_rails_...  (employee_id => employees.id)
#

class DownPayment < ApplicationRecord
  has_paper_trail
  belongs_to :employee
end
