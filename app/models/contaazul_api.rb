class ContaazulApi < ApplicationRecord
  def self.client_secret
    "W8X4qZlebacDpeFN2bwuWKkpYZNMZcqu"
  end

  def self.client_id
    "tmwT2FZ5V8MKXF8kcgqmwCuvgUrT8J4P"
  end

  def self.redirect_uri
      "https://www.veeg.com.br/oauth/contazul"
  end

  def self.url
    "https://api.contaazul.com"
  end

  def self.oauth_client
    OAuth2::Client.new(
        client_id, client_secret,
        site: self.url,
        token_url: "/oauth2/token"
    )
  end

  def self.get_token(code)
    oauth_client.auth_code.get_token(
        code,
        :redirect_uri => redirect_uri,
        :headers => {'Authorization' => 'Basic some_password'}
    )
  end

  def self.refresh_token
    begin
    config = Configuration.find_by_key("conta_token")
    code =  Base64.encode64("client_id:#{self.client_secret}")
    response = JSON.parse(RestClient.post("#{url}/oauth2/token", {
        grant_type: "refresh_token",
        refresh_token: config.value,
        client_id: client_id,
        client_secret: client_secret
    }, headers:{
        Authorization: "Basic #{code}"
    }))
    config.value = response["refresh_token"]
    config.save
    response["access_token"]
    rescue
    end
  end

  def self.call(method , endereco, params={})
    params[:access_token]= refresh_token
    begin
      if method == :get
        params_url  = URI.unescape(params.to_param)
        endereco = url + endereco +"?"+ params_url
        call = RestClient.get(endereco)
      else
        endereco = url + endereco
        call = RestClient.send(method, endereco, params.to_json, {
            :content_type => "application/json",
            :accept => "application/json"
        })
      end
    rescue RestClient::ExceptionWithResponse => e
      return {a: e.response}
    end
    return JSON.parse(call, symbolize_names: true)
  end

end
