# == Schema Information
#
# Table name: clients
#
#  id                 :integer          not null, primary key
#  nome               :string
#  cpf_cnpj           :string
#  inscricao_estadual :string
#  nome_fantasia      :string
#  cep                :string
#  endereco           :string
#  bairro             :string
#  complemento        :string
#  cidade             :string
#  estado             :string
#  email              :string
#  observacoes        :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  taghere_uid        :string
#  taghere_token      :string
#  contaazul_id       :string
#

class Client < ApplicationRecord
  has_paper_trail
  has_many :client_phones
  has_many :order_payments
  has_many :client_payments
  has_many :comandas

  accepts_nested_attributes_for :client_phones, allow_destroy: true
  accepts_nested_attributes_for :client_payments, allow_destroy: true

  after_validation :sync_contaazul

  def sync_contaazul
    if self.contaazul_id.present?
      update_on_contaazul
    else
      create_on_contaazul
    end
  end

  def update_on_contaazul
    params = contaazul_params
    response = ContaazulApi.call(:put, "/v1/customers/#{self.contaazul_id}", params)
    response
  end
  
  def create_on_contaazul
    params = contaazul_params
    response = ContaazulApi.call(:post, "/v1/customers", params)
    self.contaazul_id = response[:id]
  end

  def contaazul_params
    if self.cpf_cnpj.present? and self.cpf_cnpj.size == 11
      person_type = "NATURAL"
    else
      person_type = "LEGAL"
    end
    params = {
        "name": self.nome,
        "identity_document": self.cpf_cnpj,
        "person_type": person_type,
        "state_registration_number": self.inscricao_estadual,
        "notes": self.observacoes,
    }
  end

  def to_s
    self.nome
  end

  def saldo
    client_payments.sum(:value) - order_payments.sum(:value)
  end

  def self.get_by_app(access_token)
    data = TagHereApi.get_user(access_token)
    query = Client.where("taghere_uid = ? or email = ?", data[:uuid], data[:email])
    if query.present?
      user = query.first
    else
      user = Client.new(taghere_uid: data[:uuid], nome: data[:name], email: data[:email])
    end
    user.taghere_token = data[:access_token]
    user.save
    user
  end

  def set_table(table)
    begin
      list = comandas.where(status: false)
      if list.present?
        list.update_all(dining_table_id: table)
      else
        Comanda.create(
           dining_table_id: table,
           tipo: :app,
           entrega: false,
           status: false,
           client: self
        )
      end
      true
    rescue
      false
    end
  end
end
