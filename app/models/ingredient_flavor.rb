# == Schema Information
#
# Table name: ingredient_flavors
#
#  id            :integer          not null, primary key
#  ingredient_id :integer
#  flavor_id     :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  quantity      :integer
#
# Indexes
#
#  index_ingredient_flavors_on_flavor_id      (flavor_id)
#  index_ingredient_flavors_on_ingredient_id  (ingredient_id)
#
# Foreign Keys
#
#  fk_rails_...  (flavor_id => flavors.id)
#  fk_rails_...  (ingredient_id => ingredients.id)
#

class IngredientFlavor < ApplicationRecord
  has_paper_trail
  belongs_to :ingredient
  belongs_to :flavor
end
