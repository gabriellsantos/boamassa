json.extract! order, :id, :comanda_id, :status, :delivery_time, :delivery_duration, :created_at, :updated_at
json.url order_url(order, format: :json)
