json.extract! promotion, :id, :name, :start_at, :end_at, :created_at, :updated_at
json.url promotion_url(promotion, format: :json)
