json.extract! provider, :id, :name, :inscricao, :inscricao_estadual, :nome_fantasia, :cep, :endereco, :bairro, :cidade, :estado, :email, :observacoes, :telefones, :site, :ativo, :created_at, :updated_at
json.url provider_url(provider, format: :json)
