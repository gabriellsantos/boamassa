json.extract! entrance, :id, :created_at, :updated_at
json.url entrance_url(entrance, format: :json)
