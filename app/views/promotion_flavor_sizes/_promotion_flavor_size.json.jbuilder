json.extract! promotion_flavor_size, :id, :discount, :promotion_id, :flavor_size_id, :created_at, :updated_at
json.url promotion_flavor_size_url(promotion_flavor_size, format: :json)
