json.extract! configuration, :id, :key, :value, :created_at, :updated_at
json.url configuration_url(configuration, format: :json)
