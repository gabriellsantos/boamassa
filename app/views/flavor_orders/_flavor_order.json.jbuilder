json.extract! flavor_order, :id, :product_order_id, :flavor_size_id, :created_at, :updated_at
json.url flavor_order_url(flavor_order, format: :json)
