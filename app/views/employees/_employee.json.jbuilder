json.extract! employee, :id, :name, :address, :pis, :phone, :cpf, :rg, :hiring_date, :demission_date, :created_at, :updated_at
json.url employee_url(employee, format: :json)
