json.extract! product_size, :id, :product_id, :name, :description, :quantity_flavors, :value, :active, :created_at, :updated_at
json.url product_size_url(product_size, format: :json)
