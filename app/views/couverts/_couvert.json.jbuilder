json.extract! couvert, :id, :name, :value, :created_at, :updated_at
json.url couvert_url(couvert, format: :json)
