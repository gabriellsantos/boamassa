json.extract! client_phone, :id, :phone, :client_id, :created_at, :updated_at
json.url client_phone_url(client_phone, format: :json)
