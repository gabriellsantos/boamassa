json.extract! subgroup, :id, :name, :product_group_id, :created_at, :updated_at
json.url subgroup_url(subgroup, format: :json)
