json.extract! flavor_size, :id, :flavor_id, :product_size_id, :value, :active, :created_at, :updated_at
json.url flavor_size_url(flavor_size, format: :json)
