json.extract! client, :id, :nome, :cpf_cnpj, :inscricao_estadual, :nome_fantasia, :cep, :endereco, :bairro, :complemento, :cidade, :estado, :email, :observacoes, :created_at, :updated_at
json.url client_url(client, format: :json)
