json.extract! ingredient_flavor, :id, :ingredient_id, :flavor_id, :created_at, :updated_at
json.url ingredient_flavor_url(ingredient_flavor, format: :json)
