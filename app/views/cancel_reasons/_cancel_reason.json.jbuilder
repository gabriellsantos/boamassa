json.extract! cancel_reason, :id, :name, :created_at, :updated_at
json.url cancel_reason_url(cancel_reason, format: :json)
