json.extract! ingredient_entrace, :id, :entrance_id, :ingredient_id, :value, :created_at, :updated_at
json.url ingredient_entrace_url(ingredient_entrace, format: :json)
