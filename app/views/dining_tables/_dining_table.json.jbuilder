json.extract! dining_table, :id, :number, :beacon, :status, :created_at, :updated_at
json.url dining_table_url(dining_table, format: :json)
