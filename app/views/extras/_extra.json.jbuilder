json.extract! extra, :id, :product_size_id, :flavor_id, :value, :active, :created_at, :updated_at
json.url extra_url(extra, format: :json)
