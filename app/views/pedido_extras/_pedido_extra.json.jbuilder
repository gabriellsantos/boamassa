json.extract! pedido_extra, :id, :product_order_id, :extra_id, :value, :created_at, :updated_at
json.url pedido_extra_url(pedido_extra, format: :json)
