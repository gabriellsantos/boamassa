class ProductsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :set_combos, only: [:new,:edit,:create,:update]

  # GET /products
  def index
    @q = Product.all.ransack(params[:q])
    @products = @q.result.page(params[:page])
  end

  # GET /products/1
  def show
  end



  # GET /products/new
  def new
    @product = Product.new
    @product.active = true
  end

  # GET /products/1/edit
  def edit

  end

  # POST /products
  def create
    @product = Product.new(product_params)

    if @product.save
      redirect_to @product, notice: 'Categoria criada com sucesso!'
    else
      render :new
    end
  end

  # PATCH/PUT /products/1
  def update
    if @product.update(product_params)
      redirect_to @product, notice: 'Categoria atualizada com sucesso!'
    else
      render :edit
    end
  end

  # DELETE /products/1
  def destroy
    begin
      @product.destroy
      redirect_to products_url, notice: 'Categoria atualizada com sucesso!'
    rescue
      redirect_to products_url, alert: 'Categoria já está vinculada!'
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

  def set_combos

    @groups = Subgroup.all.sort_by(&:name).map{|e| [e.name,e.id]}
    @areas = Area.all.sort_by(&:name).map{|e| [e.name,e.id]}
    @calculos = Product.calculo.options
    @ingredients = Ingredient.all.sort_by{|e| e.name }.map{|a| [a.name,a.id]}
    @flavors = Flavor.all.sort_by{|e| e.name }.map{|a| [a.name,a.id]}
  end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product).permit(:name, :description, :photo, :subgroup_id, :area_id, :calculo, :active,
      product_sizes_attributes:[:id,:name,:description,:quantity_flavors,:value,:active,:_destroy,
      flavors_attributes:[:id,:product_id,:value,:visible,:active,:name, :description,:_destroy,],

      flavor_extras_attributes:[:id,:product_id,:value,:visible,:active,:name, :description,:_destroy]
       ])
    end
end
