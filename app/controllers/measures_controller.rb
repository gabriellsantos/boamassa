class MeasuresController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_measure, only: [:show, :edit, :update, :destroy]

  # GET /measures
  def index
    @q = Measure.all.ransack(params[:q])
    @measures = @q.result.page(params[:page])
  end

  # GET /measures/1
  def show
  end

  # GET /measures/new
  def new
    @measure = Measure.new
  end

  # GET /measures/1/edit
  def edit
  end

  # POST /measures
  def create
    @measure = Measure.new(measure_params)

    if @measure.save
      redirect_to @measure, notice: 'Unidade de Medida cadastrada com sucesso!'
    else
      render :new
    end
  end

  # PATCH/PUT /measures/1
  def update
    if @measure.update(measure_params)
      redirect_to @measure, notice: 'Unidade de Medida atualizada com sucesso!'
    else
      render :edit
    end
  end

  # DELETE /measures/1
  def destroy
    begin
      @measure.destroy
      redirect_to measures_url, notice: 'Unidade de Medida excluída com sucesso!'
    rescue
      redirect_to measures_url, alert: 'Unidade de Medida já está vinculada!'
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_measure
      @measure = Measure.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def measure_params
      params.require(:measure).permit(:name, :sigla)
    end
end
