class SubgroupsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_subgroup, only: [:show, :edit, :update, :destroy]

  # GET /subgroups
  def index
    @q = Subgroup.all.ransack(params[:q])
    @subgroups = @q.result.page(params[:page])
  end

  # GET /subgroups/1
  def show
  end

  # GET /subgroups/new
  def new
    @subgroup = Subgroup.new
  end

  # GET /subgroups/1/edit
  def edit
  end

  # POST /subgroups
  def create
    @subgroup = Subgroup.new(subgroup_params)

    if @subgroup.save
      redirect_to @subgroup, notice: 'Subgroup was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /subgroups/1
  def update
    if @subgroup.update(subgroup_params)
      redirect_to @subgroup, notice: 'Subgroup was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /subgroups/1
  def destroy
    @subgroup.destroy
    redirect_to subgroups_url, notice: 'Subgroup was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subgroup
      @subgroup = Subgroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def subgroup_params
      params.require(:subgroup).permit(:name, :product_group_id)
    end
end
