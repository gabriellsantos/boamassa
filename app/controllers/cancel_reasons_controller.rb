class CancelReasonsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_cancel_reason, only: [:show, :edit, :update, :destroy]

  # GET /cancel_reasons
  def index
    @q = CancelReason.all.ransack(params[:q])
    @cancel_reasons = @q.result.page(params[:page])
  end

  # GET /cancel_reasons/1
  def show
  end

  # GET /cancel_reasons/new
  def new
    @cancel_reason = CancelReason.new
  end

  # GET /cancel_reasons/1/edit
  def edit
  end

  # POST /cancel_reasons
  def create
    @cancel_reason = CancelReason.new(cancel_reason_params)

    if @cancel_reason.save
      redirect_to @cancel_reason, notice: 'Motivo de Cancelamento criado com sucesso'
    else
      render :new
    end
  end

  # PATCH/PUT /cancel_reasons/1
  def update
    if @cancel_reason.update(cancel_reason_params)
      redirect_to @cancel_reason, notice: 'Motivo de Cancelamento atualizado com sucesso'
    else
      render :edit
    end
  end

  # DELETE /cancel_reasons/1
  def destroy
    begin
      @cancel_reason.destroy
      redirect_to cancel_reasons_url, notice: 'Motivo excluído com sucesso!'
    rescue
      redirect_to cancel_reasons_path, alert: "Esse motivo já está vinculado!"
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cancel_reason
      @cancel_reason = CancelReason.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cancel_reason_params
      params.require(:cancel_reason).permit(:name)
    end
end
