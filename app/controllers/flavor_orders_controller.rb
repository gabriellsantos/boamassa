class FlavorOrdersController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_flavor_order, only: [:show, :edit, :update, :destroy]

  # GET /flavor_orders
  def index
    @q = FlavorOrder.all.ransack(params[:q])
    @flavor_orders = @q.result.page(params[:page])
  end

  # GET /flavor_orders/1
  def show
  end

  # GET /flavor_orders/new
  def new
    @flavor_order = FlavorOrder.new
  end

  # GET /flavor_orders/1/edit
  def edit
  end

  # POST /flavor_orders
  def create
    @flavor_order = FlavorOrder.new(flavor_order_params)

    if @flavor_order.save
      redirect_to @flavor_order, notice: 'Produto de Saída criado com sucesso!'
    else
      render :new
    end
  end

  # PATCH/PUT /flavor_orders/1
  def update
    if @flavor_order.update(flavor_order_params)
      redirect_to @flavor_order, notice: 'Produto de Saída atualizado com sucesso!'
    else
      render :edit
    end
  end

  # DELETE /flavor_orders/1
  def destroy
    begin
      @flavor_order.destroy
      redirect_to flavor_orders_url, notice: 'Produto de Saída excluído com sucesso!'
    rescue
      redirect_to flavor_orders_url, alert: 'Produto de Saída já está vinculado!'
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_flavor_order
      @flavor_order = FlavorOrder.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def flavor_order_params
      params.require(:flavor_order).permit(:product_order_id, :flavor_size_id)
    end
end
