class PromotionsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_promotion, only: [:show, :edit, :update, :destroy]
  before_action :set_combos, only: [:new, :edit, :create]

  # GET /promotions
  def index
    @q = Promotion.all.ransack(params[:q])
    @promotions = @q.result.page(params[:page])
  end

  # GET /promotions/1
  def show
  end

  # GET /promotions/new
  def new
    @promotion = Promotion.new
  end

  # GET /promotions/1/edit
  def edit
  end

  # POST /promotions
  def create
    @promotion = Promotion.new(promotion_params)

    if @promotion.save
      redirect_to @promotion, notice: 'Promoção criada com sucesso!'
    else
      render :new
    end
  end

  # PATCH/PUT /promotions/1
  def update
    if @promotion.update(promotion_params)
      redirect_to @promotion, notice: 'Promoção atualizada com sucesso!'
    else
      render :edit
    end
  end

  # DELETE /promotions/1
  def destroy
    @promotion.destroy
    redirect_to promotions_url, notice: 'Promoção excluída com sucesso!'
  end

  private

  def set_combos
    @flavors = FlavorSize.includes(:flavor,:product_size,:product).all.sort_by{|a| a.product.name}.map{|a| ["#{a.product.name} - #{a.flavor} - #{a.product_size.name}", a.id]}
    @groups = Subgroup.all.sort_by{|a| a}.map{|a| [a, a.id]}
  end
    # Use callbacks to share common setup or constraints between actions.
    def set_promotion
      @promotion = Promotion.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def promotion_params
      params.require(:promotion).permit(:name, :start_at, :end_at,
      promotion_flavor_sizes_attributes:[:id,:discount,:promotion_id,:flavor_size_id, :_destroy],
      promotion_groups_attributes:[:id,:discount,:promotion_id,:subgroup_id,:_destroy])
    end
end
