class PedidoExtrasController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_pedido_extra, only: [:show, :edit, :update, :destroy]

  # GET /pedido_extras
  def index
    @q = PedidoExtra.all.ransack(params[:q])
    @pedido_extras = @q.result.page(params[:page])
  end

  # GET /pedido_extras/1
  def show
  end

  # GET /pedido_extras/new
  def new
    @pedido_extra = PedidoExtra.new
  end

  # GET /pedido_extras/1/edit
  def edit
  end

  # POST /pedido_extras
  def create
    @pedido_extra = PedidoExtra.new(pedido_extra_params)

    if @pedido_extra.save
      redirect_to @pedido_extra, notice: 'Pedido extra was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /pedido_extras/1
  def update
    if @pedido_extra.update(pedido_extra_params)
      redirect_to @pedido_extra, notice: 'Pedido extra was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /pedido_extras/1
  def destroy
    @pedido_extra.destroy
    redirect_to pedido_extras_url, notice: 'Pedido extra was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pedido_extra
      @pedido_extra = PedidoExtra.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def pedido_extra_params
      params.require(:pedido_extra).permit(:product_order_id, :extra_id, :value)
    end
end
