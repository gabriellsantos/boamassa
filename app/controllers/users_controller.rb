class UsersController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  skip_before_action :verify_authenticity_token
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :set_combos, only:[:new,:create,:edit,:update]

  def index
    @q = User.all.ransack(params[:q])
    @users = @q.result.page(params[:page])
  end

  def show
    
  end

  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to @user, notice: 'Usuário criado com sucesso!'
    else
      render :new
    end
  end

  # PATCH/PUT /products/1
  def update
    if @user.update(user_params)
      redirect_to @user, notice: 'Usuário atualizado com sucesso.'
    else
      render :edit
    end
  end

  # DELETE /products/1
  def destroy
    begin
      @user.destroy
      redirect_to user_url, notice: 'Usuário deletado com sucesso.'
    rescue
      redirect_to user_url, notice: 'Usuário já está vinculado!'
    end

  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  def set_combos
    @perfis = User.role.options
  end

  # Only allow a trusted parameter "white list" through.
  def user_params
    params.require(:user).permit(:name,:email,:password,:confirmation_password,:role)
  end
end
