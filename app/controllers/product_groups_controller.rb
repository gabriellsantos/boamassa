class ProductGroupsController < ApplicationController
  before_action :authenticate_user!, except: [:index]
  load_and_authorize_resource
  before_action :set_product_group, only: [:show, :edit, :update, :destroy]

  # GET /product_groups
  def index
    @q = ProductGroup.all.ransack(params[:q])
    respond_to do |format|
      format.html{@product_groups = @q.result.page(params[:page])}
      format.json{render json: ProductGroup.includes(:subgroups).to_json(include: :subgroups)}

    end

  end

  # GET /product_groups/1
  def show
  end

  # GET /product_groups/new
  def new
    @product_group = ProductGroup.new
  end

  # GET /product_groups/1/edit
  def edit
  end

  # POST /product_groups
  def create
    @product_group = ProductGroup.new(product_group_params)

    if @product_group.save
      redirect_to @product_group, notice: 'Grupo criado com sucesso!'
    else
      render :new
    end
  end

  # PATCH/PUT /product_groups/1
  def update
    if @product_group.update(product_group_params)
      redirect_to @product_group, notice: 'Grupo atualizado com sucesso!'
    else
      render :edit
    end
  end

  # DELETE /product_groups/1
  def destroy
    begin
      @product_group.destroy
      redirect_to product_groups_url, notice: 'Grupo excluído com sucesso!'
    rescue
      redirect_to product_groups_url, alert: 'Grupo já está vinculado!'
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_group
      @product_group = ProductGroup.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_group_params
      params.require(:product_group).permit(:name,subgroups_attributes:[:id,:name,:product_group_id,:_destroy])
    end
end
