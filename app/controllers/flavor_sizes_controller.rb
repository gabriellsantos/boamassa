class FlavorSizesController < ApplicationController
  before_action :authenticate_user!, except: [:produtos_por_categoria,:retorna_adicionais,:retorna_sabores]
  load_and_authorize_resource
  before_action :set_flavor_size, only: [:show, :edit, :update, :destroy]

  # GET /flavor_sizes
  def index
    @q = FlavorSize.all.ransack(params[:q])
    @flavor_sizes = @q.result.page(params[:page])
  end

  # GET /flavor_sizes/1
  def show
    respond_to do |format|
      format.html
      format.json
    end
  end

  def produtos_por_categoria
    render json: FlavorSize.includes(:product, :flavor).order("flavors.name asc").where(products:{subgroup_id: params[:subgroup]},active:true, visible: true).to_json
  end

  def retorna_sabores
    #render json: Product.find(params[:product]).flavor_sizes.where(active: true, visible: false).to_json
    render json: ProductSize.find(params[:product]).flavor_sizes.where(active: true, visible: false).to_json
  end

  def retorna_adicionais
    #render json: Product.find(params[:product]).flavor_sizes.where(active: true, visible: false).to_json
    render json: ProductSize.find(params[:product]).extras.where(active: true).to_json
  end

  # GET /flavor_sizes/new
  def new
    @flavor_size = FlavorSize.new
  end

  # GET /flavor_sizes/1/edit
  def edit
  end

  # POST /flavor_sizes
  def create
    @flavor_size = FlavorSize.new(flavor_size_params)

    if @flavor_size.save
      redirect_to @flavor_size, notice: 'Tamanho criado com sucesso!'
    else
      render :new
    end
  end

  # PATCH/PUT /flavor_sizes/1
  def update
    if @flavor_size.update(flavor_size_params)
      redirect_to @flavor_size, notice: 'Tamanho atualizado com sucesso!'
    else
      render :edit
    end
  end

  # DELETE /flavor_sizes/1
  def destroy
    @flavor_size.destroy
    redirect_to flavor_sizes_url, notice: 'Tamanho excluído com sucesso!'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_flavor_size
      @flavor_size = FlavorSize.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def flavor_size_params
      params.require(:flavor_size).permit(:flavor_id, :product_size_id, :value, :active)
    end
end
