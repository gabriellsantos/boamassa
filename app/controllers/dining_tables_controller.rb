class DiningTablesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_dining_table, only: [:show, :edit, :update, :destroy, :comandas,:unificar_comandas]

  # GET /dining_tables
  def index
    @q = DiningTable.all.ransack(params[:q])
    @dining_tables = @q.result.page(params[:page])
  end

  # GET /dining_tables/1
  def show
  end

  def home
    @token = TagHereApi.get_firebase_token
    @dining_tables = DiningTable.all
  end

  def comandas
    @comandas = Comanda.where(dining_table: @dining_table, status: false)
  end

  def unificar_comandas
    comandas = @dining_table.comandas.where("status <> true")
    comandas.each do |c|
      c.orders.update(comanda_id: params[:comanda])
      c.order_payments.update(comanda_id: params[:comanda])
    end
    comanda = Comanda.find(params[:comanda])
    comandas.where.not(id: comanda.id).destroy_all
    redirect_to comanda_path(comanda)
  end

  # GET /dining_tables/new
  def new
    @dining_table = DiningTable.new
  end

  # GET /dining_tables/1/edit
  def edit
  end

  # POST /dining_tables
  def create
    @dining_table = DiningTable.new(dining_table_params)

    if @dining_table.save
      redirect_to @dining_table, notice: 'Mesa criada com sucesso!'
    else
      render :new
    end
  end

  # PATCH/PUT /dining_tables/1
  def update
    if @dining_table.update(dining_table_params)
      redirect_to @dining_table, notice: 'Mesa atualizada com sucesso!'
    else
      render :edit
    end
  end

  # DELETE /dining_tables/1
  def destroy
    @dining_table.destroy
    redirect_to dining_tables_url, notice: 'Mesa excluída com sucesso!'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dining_table
      @dining_table = DiningTable.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def dining_table_params
      params.require(:dining_table).permit(:number, :beacon, :status)
    end
end
