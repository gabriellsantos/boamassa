class ProvidersController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_provider, only: [:show, :edit, :update, :destroy]

  # GET /providers
  def index
    @q = Provider.all.order(name: :asc).ransack(params[:q])
    @providers = @q.result.page(params[:page])
  end

  def check_cpf
    @provider = Provider.find_by_inscricao(params[:inscricao])
    if @provider.present?
      render json: {status: true, person: @provider, type: :user }
    else
      if params[:inscricao].length > 11
        api = CnpjApi.new(params[:inscricao])
      else
        api = CpfApi.new(params[:inscricao])
      end
      render json: {status: api.status == 1, person: api, type: :api}
    end
  end

  # GET /providers/1
  def show
  end

  # GET /providers/new
  def new
    @provider = Provider.new
  end

  # GET /providers/1/edit
  def edit
  end

  # POST /providers
  def create
    @provider = Provider.new(provider_params)

    if @provider.save
      redirect_to @provider, notice: 'Fornecedor criado com sucesso!'
    else
      render :new
    end
  end

  # PATCH/PUT /providers/1
  def update
    if @provider.update(provider_params)
      redirect_to @provider, notice: 'Fornecedor atualizado com sucesso!'
    else
      render :edit
    end
  end

  # DELETE /providers/1
  def destroy
    begin
      @provider.destroy
      redirect_to providers_url, notice: 'Fornecedor excluído com sucesso!'
    rescue
      redirect_to providers_url, alert: 'Fornecedor já está vinculado!'
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_provider
      @provider = Provider.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def provider_params
      params.require(:provider).permit(:name,:numero,:complemento, :inscricao, :inscricao_estadual, :nome_fantasia, :cep, :endereco, :bairro, :cidade, :estado, :email, :observacoes, :telefones, :site, :ativo)
    end
end
