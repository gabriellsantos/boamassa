class TaghereController < ApplicationController
  def menu
    render json: {
        options: [
            {
                type: "qr_code",
                title: "Selecionar Mesa",
                presence: true
            },

            {
                type: "remote",
                title: "Chamar o Garçon",
                presence: true,
                url: "https://boamassa.fwd.wf/taghere/garcon"
            },

            {
                type: "frame",
                title: "Fazer Um Pedido",
                presence: true,
                url: "https://boamassa.fwd.wf/taghere/order"
            }


        ]
    }
  end

  def set_table
    begin
      if @customer_app.present?
        @table = DiningTable.find(params[:region])
        if @customer_app.set_table(@table.id)
          render json: {action: {type: "alert", title: "Deu Certo!", message:"Agora você pode realizar seu pedido ;)"}}
        else
          alert_error
        end
      else
        alert_error
      end
    rescue
      alert_error
    end

  end

  def order
    if @customer_app.present?
      @comandas = @customer_app.comandas.actives
      if @comandas.present?
        @comanda = @comandas.last
      else
        render "error"
      end
    end
  end

  def balance
  end

  def success
  end

  def garcon
    if @customer_app.present?
      @comandas = @customer_app.comandas.actives
      if @comandas.present?
        @comanda = @comandas.last
        render json: {action: {
            type: "request",
            title: "Chamando o Garçon",
            description: "Garçon Solicitado",
            status: "Solicitado",
            active: true,
            location: @comanda.dining_table.number,
            category: "garçon"
        }}
      else
        render json: {action: {
            type: "alert",
            title: "Selecione uma mesa",
            message:"Antes de realizar essa ação, vocÊ precisa dizer em qual mesa está"
        }}
      end
    else
      alert_error
    end
  end

  private
  def alert_error
    render json: {action: {type: "alert", title: "Foi Mal", message:"Não conseguimos realizar essa ação. Tente novamente ou peça auxílio ao garçon"}}
  end
end
