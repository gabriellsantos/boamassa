class EntrancesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_entrance, only: [:show, :edit, :update, :destroy]
  before_action :set_combos, only: [:new,:edit,:create]

  # GET /entrances
  def index
    @q = Entrance.all.ransack(params[:q])
    @entrances = @q.result.page(params[:page])
  end

  # GET /entrances/1
  def show
  end

  # GET /entrances/new
  def new
    @entrance = Entrance.new

  end

  # GET /entrances/1/edit
  def edit
  end

  # POST /entrances
  def create
    @entrance = Entrance.new(entrance_params)

    if @entrance.save
      redirect_to @entrance, notice: 'Entrada criada com sucesso!'
    else
      render :new
    end
  end

  # PATCH/PUT /entrances/1
  def update
    if @entrance.update(entrance_params)
      redirect_to @entrance, notice: 'Entrada atualizada com sucesso!'
    else
      render :edit
    end
  end

  # DELETE /entrances/1
  def destroy
    begin
      @entrance.destroy
      redirect_to entrances_url, notice: 'Entrada excluída com sucesso!'
    rescue
      redirect_to entrances_url, notice: 'Entrada já está vinculada!'
    end

  end

  private
  def set_combos
    @ingredients = Ingredient.all.sort_by{|e| e.name }.map{|a| [a.name,a.id]}
    @providers = Provider.all.sort_by{|e| e.name }.map{|a| [a.name,a.id]}
  end

    # Use callbacks to share common setup or constraints between actions.
    def set_entrance
      @entrance = Entrance.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def entrance_params
      params.require(:entrance).permit(:provider_id,ingredient_entraces_attributes:[:id,:entrance_id,:ingredient_id,:value,:quantity])
    end
end
