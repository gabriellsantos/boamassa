class GoogleController < ApplicationController
  def callback
    PrintApi.generate_token(params[:code])
    redirect_to root_path
  end

  def redirect
    redirect_to PrintApi.client(true).auth.generate_url(PrintApi.redirect_uri)
  end

  def jobs
    @jobs = PrintApi.client.print_jobs.all
  end
end
