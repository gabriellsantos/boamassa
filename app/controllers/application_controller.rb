class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :get_customer_by_app
  before_action :set_paper_trail_whodunnit

  #layout :layout_by_resource

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to "/403.html", :alert => exception.message
  end

  def get_customer_by_app
    if params[:access_token].present?
      @customer_app = Client.get_by_app(params[:access_token])
    end
  end

  private

  def layout_by_resource
    if devise_controller?
      "clean"
    else
      "application"
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
  end
end
