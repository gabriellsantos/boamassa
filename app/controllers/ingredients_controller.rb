class IngredientsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_ingredient, only: [:show, :edit, :update, :destroy]
  before_action :set_combos, only:[:new,:create,:edit]

  # GET /ingredients
  def index
    @q = Ingredient.all.ransack(params[:q])
    @ingredients = @q.result.page(params[:page])
  end

  # GET /ingredients/1
  def show
  end

  # GET /ingredients/new
  def new
    @ingredient = Ingredient.new
  end

  # GET /ingredients/1/edit
  def edit
  end

  # POST /ingredients
  def create
    @ingredient = Ingredient.new(ingredient_params)

    if @ingredient.save
      redirect_to @ingredient, notice: 'Produto de Entrada cadastrado com sucesso!'
    else
      render :new
    end
  end

  # PATCH/PUT /ingredients/1
  def update
    if @ingredient.update(ingredient_params)
      redirect_to @ingredient, notice: 'Produto de Entrada atualizado com sucesso!'
    else
      render :edit
    end
  end

  # DELETE /ingredients/1
  def destroy
    begin
      @ingredient.destroy
      redirect_to ingredients_url, notice: 'Produto de Entrada atualizado com sucesso!'
    rescue
      redirect_to ingredients_url, notice: 'Produto de Entrada já está vinculado!'
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ingredient
      @ingredient = Ingredient.find(params[:id])
    end

  def set_combos
    @measures = Measure.all.sort_by{|a| a.sigla}.map{|a| ["#{a.name} - #{a.sigla}",a.id]}
  end

    # Only allow a trusted parameter "white list" through.
    def ingredient_params
      params.require(:ingredient).permit(:name, :description, :quantity,:measure_id)
    end
end
