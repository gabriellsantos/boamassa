class ClientsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_client, only: [:show,:crediario, :edit, :update, :destroy]

  # GET /clients
  def index
    @q = Client.all.ransack(params[:q])
    @clients = @q.result.page(params[:page])
  end

  def check_cpf
    @client = Client.find_by_cpf_cnpj(params[:inscricao])
    if @client.present?
      render json: {status: true, person: @client, type: :user }
    else
      if params[:inscricao].length > 11
        api = CnpjApi.new(params[:inscricao])
      else
        api = CpfApi.new(params[:inscricao])
      end

      render json: {status: api.status == 1, person: api, type: :api}
    end
  end

  def crediario
    @q = @client.order_payments.ransack(params[:q])
    @creditos = @q.result
  end

  def credito
    @pagamento = OrderPayment.find(params[:pagamento])
    respond_to do |format|
      format.html
      format.text
      format.pdf do
        render pdf: "teste"
      end
    end
  end

  # GET /clients/1
  def show
    @creditos = OrderPayment.where(client: @client)
  end

  # GET /clients/new
  def new
    @client = Client.new
  end

  # GET /clients/1/edit
  def edit
  end

  # POST /clients
  def create
    @client = Client.new(client_params)

    if @client.save
      redirect_to @client, notice: 'Cliente criado com sucesso!'
    else
      render :new
    end
  end

  # PATCH/PUT /clients/1
  def update
    if @client.update(client_params)
      redirect_to @client, notice: 'Cliente atualizado com sucesso!'
    else
      render :edit
    end
  end

  # DELETE /clients/1
  def destroy
    begin
    @client.destroy
      redirect_to clients_url, notice: 'Cliente excluído com sucesso!'
    rescue
      redirect_to clients_url, notice: 'Cliente já está vinculado!'
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def client_params
      params.require(:client).permit(:nome, :cpf_cnpj, :inscricao_estadual, :nome_fantasia, :cep, :endereco, :bairro, :complemento, :cidade, :estado, :email, :observacoes,
      client_phones_attributes:[:id,:client_id,:phone,:_dentroy],
      client_payments_attributes:[:id,:client_id,:value,:_destroy])
    end
end
