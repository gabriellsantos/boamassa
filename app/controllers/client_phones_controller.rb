class ClientPhonesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_client_phone, only: [:show, :edit, :update, :destroy]

  # GET /client_phones
  def index
    @q = ClientPhone.all.ransack(params[:q])
    @client_phones = @q.result.page(params[:page])
  end

  # GET /client_phones/1
  def show
  end

  # GET /client_phones/new
  def new
    @client_phone = ClientPhone.new
  end

  # GET /client_phones/1/edit
  def edit
  end

  # POST /client_phones
  def create
    @client_phone = ClientPhone.new(client_phone_params)

    if @client_phone.save
      redirect_to @client_phone, notice: 'Telefone do Cliente criado com sucesso!'
    else
      render :new
    end
  end

  # PATCH/PUT /client_phones/1
  def update
    if @client_phone.update(client_phone_params)
      redirect_to @client_phone, notice: 'Telefone do Cliente atualizado com sucesso!'
    else
      render :edit
    end
  end

  # DELETE /client_phones/1
  def destroy
    @client_phone.destroy
    redirect_to client_phones_url, notice: 'Telefone do Cliente excluído com sucesso!'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client_phone
      @client_phone = ClientPhone.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def client_phone_params
      params.require(:client_phone).permit(:phone, :client_id)
    end
end
