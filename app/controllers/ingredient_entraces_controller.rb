class IngredientEntracesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_ingredient_entrace, only: [:show, :edit, :update, :destroy]

  # GET /ingredient_entraces
  def index
    @q = IngredientEntrace.all.ransack(params[:q])
    @ingredient_entraces = @q.result.page(params[:page])
  end

  # GET /ingredient_entraces/1
  def show
  end

  # GET /ingredient_entraces/new
  def new
    @ingredient_entrace = IngredientEntrace.new
  end

  # GET /ingredient_entraces/1/edit
  def edit
  end

  # POST /ingredient_entraces
  def create
    @ingredient_entrace = IngredientEntrace.new(ingredient_entrace_params)

    if @ingredient_entrace.save
      redirect_to @ingredient_entrace, notice: 'Ingredient entrace was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /ingredient_entraces/1
  def update
    if @ingredient_entrace.update(ingredient_entrace_params)
      redirect_to @ingredient_entrace, notice: 'Ingredient entrace was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /ingredient_entraces/1
  def destroy
    @ingredient_entrace.destroy
    redirect_to ingredient_entraces_url, notice: 'Ingredient entrace was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ingredient_entrace
      @ingredient_entrace = IngredientEntrace.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ingredient_entrace_params
      params.require(:ingredient_entrace).permit(:entrance_id, :ingredient_id, :value)
    end
end
