class IngredientFlavorsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_ingredient_flavor, only: [:show, :edit, :update, :destroy]

  # GET /ingredient_flavors
  def index
    @q = IngredientFlavor.all.ransack(params[:q])
    @ingredient_flavors = @q.result.page(params[:page])
  end

  # GET /ingredient_flavors/1
  def show
  end

  # GET /ingredient_flavors/new
  def new
    @ingredient_flavor = IngredientFlavor.new
  end

  # GET /ingredient_flavors/1/edit
  def edit
  end

  # POST /ingredient_flavors
  def create
    @ingredient_flavor = IngredientFlavor.new(ingredient_flavor_params)

    if @ingredient_flavor.save
      redirect_to @ingredient_flavor, notice: 'Ingredient flavor was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /ingredient_flavors/1
  def update
    if @ingredient_flavor.update(ingredient_flavor_params)
      redirect_to @ingredient_flavor, notice: 'Ingredient flavor was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /ingredient_flavors/1
  def destroy
    @ingredient_flavor.destroy
    redirect_to ingredient_flavors_url, notice: 'Ingredient flavor was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ingredient_flavor
      @ingredient_flavor = IngredientFlavor.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ingredient_flavor_params
      params.require(:ingredient_flavor).permit(:ingredient_id, :flavor_id)
    end
end
