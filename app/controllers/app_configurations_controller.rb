class AppConfigurationsController < ApplicationController
  before_action :set_configuration, only: [:show, :edit, :update, :destroy]

  # GET /configurations
  def index
    @q = AppConfiguration.ransack(params[:q])
    @configurations = @q.result.page(params[:page])
  end

  # GET /configurations/1
  def show
  end

  # GET /configurations/new
  def new
    @configuration = AppConfiguration.new
  end

  # GET /configurations/1/edit
  def edit
  end

  # POST /configurations
  def create
    @configuration = AppConfiguration.new(configuration_params)

    if @configuration.save
      redirect_to @configuration, notice: 'AppConfiguration was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /configurations/1
  def update
    if @configuration.update(configuration_params)
      redirect_to @configuration, notice: 'AppConfiguration was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /configurations/1
  def destroy
    @configuration.destroy
    redirect_to configurations_url, notice: 'AppConfiguration was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_configuration
      @configuration = AppConfiguration.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def configuration_params
      params.require(:app_configuration).permit(:key, :value)
    end
end
