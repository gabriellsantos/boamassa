class ComandasController < ApplicationController
  before_action :authenticate_user!, except: [:imprimir]
  load_and_authorize_resource except: [:imprimir]
  before_action :set_comanda, only: [:show, :edit, :update, :destroy,:novo_pedido,:concluir,:imprimir,:calcular_taxa,:inserir_couvert,:fechar,:calcular_desconto]
  before_action :set_combos, only: [:new, :edit, :create,:update]

  # GET /comandas
  def index
    if params[:receber].present?
      @q = Comanda.where("id in (?)",params[:receber].tr("[]",'').split(",")).order(id: :desc).ransack(params[:q])
    else
      @q = Comanda.all.order(id: :desc).ransack(params[:q])
    end

    @comandas = @q.result.page(params[:page])
  end

  def caixa
    @q = Comanda.where(fechada: true, status: false).ransack(params[:q])
    @comandas = @q.result.page(params[:page])
  end

  # GET /comandas/1
  def show
    @comandas = Comanda.where(status: false).sort_by(&:id).map{|a| ["Mesa: #{a.dining_table} - Comanda: #{a.id}",a.id]}
    @motivos = CancelReason.all.sort_by(&:name).map{|a| [a.name,a.id]}
    #@flavors = FlavorSize.includes(:flavor,:product_size,:product).where(product_sizes: {quantity_flavors: 0}).where(active:true, visible:true).sort_by{|a| a.product.name}.map{|a| ["#{a.product.name} - #{a.flavor} - #{a.product_size.name}", a.id]}
    @flavors = FlavorSize.includes(:flavor,:product_size,:product).where(active:true, visible:true).sort_by{|a| a.product.name}.map{|a| ["#{a.product.name} - #{a.flavor} - #{a.product_size.name}", a.id]}
    @clients = Client.all.sort_by{|e| e.nome}.map{|a| [a,a.id]}
    @payments = PaymentType.all.sort_by{|e| e.name}.map{|a| [a,a.id]}
    @payments_value = @comanda.order_payments.sum(:value)
  end

  def inserir_couvert
    @comanda.couvert_value = params[:valor]
    @comanda.couvert_quantity = params[:quantity]
    @comanda.save
    redirect_to @comanda
  end

  def imprimir
    @product_orders = @comanda.product_orders.where.not(status: :cancelado)
    @crediarios = @comanda.order_payments.includes(:payment_type).where(payment_types: {name: 'Crediário'})
    #@comanda.print_cloud
    respond_to do |format|
      format.html {render layout: false}
      format.text
      format.pdf do
        render pdf: "teste"
      end
    end
  end

  def imprimir_nogroup
    @product_orders = @comanda.product_orders.where.not(status: :cancelado)
    respond_to do |format|
      format.html {render layout: false}
      format.text
      format.pdf do
        render pdf: "teste"
      end
    end
  end

  def simples
    @clients = Client.all.sort_by{|e| e.nome}.map{|a| [a,a.id]}
    @payments = PaymentType.all.sort_by{|e| e.name}.map{|a| [a,a.id]}
    @comanda = Comanda.new(tipo: :caixa, tax: 0,fechada: false)
    @comanda.orders.build
    @flavors = FlavorSize.includes(:flavor,:product_size,:product).where(active:true, visible:true).sort_by{|a| a.product.name}.map{|a| ["#{a.product.name} - #{a.flavor} - #{a.product_size.name}", a.id]}
  end

  def calcular_taxa
    if params[:no_tax].present?
      @comanda.tipo = :caixa
      @comanda.tax = 0
    else
      @comanda.tipo = :garcom
    end
    @comanda.set_tax
    @comanda.save
    redirect_to @comanda
  end

  def pedido_simples
    order = Order.create(comanda: @comanda, user: current_user)
    product_order = ProductOrder.create(flavor_size_id: params[:flavor_size_id], order: order, quantity: params[:quantity].gsub(',','.'))
    redirect_to @comanda
  end

  def calcular_desconto
    if params[:value].present?
      params[:value].sub! ',','.'
      begin
        couvert = @comanda.couvert_value * @comanda.couvert_quantity
      rescue
        couvert = 0
      end
      if params[:value].include? "%"
        @comanda.desconto = ((@comanda.product_orders.where.not(status: :cancelado).sum(&:value) + (@comanda.tax || 0) + couvert) / 100) * params[:value].to_f
      else
        @comanda.desconto = params[:value].to_f
      end
      @comanda.save
    end

    redirect_to @comanda

  end

  def dashboard
    if !params[:dia].present?
      @q = Comanda.ransack(params[:q])

    else
      @q = Comanda.where("date(created_at) = ?", DateTime.now.to_date).ransack(params[:q])
    end

    @comandas = @q.result

    @taxa = @comandas.sum{|a| a.tax}
    @recebido = @comandas.sum{|a| a.order_payments.sum(:value)}
    @areceber = @comandas.sum{|a| a.product_orders.where.not(status: :cancelado).sum(:value) + (a.couvert_calculado) + (a.tax || 0) - (a.desconto || 0)} - @recebido
    @comandas_areceber = []
    @comandas.each do |a|
      if (a.product_orders.where.not(status: :cancelado).sum(:value) + (a.couvert_calculado) + (a.tax || 0) - (a.desconto || 0)) != a.order_payments.sum(:value)
        @comandas_areceber << a.id
      end
    end
    @comandas_abertas = @comandas.where(status: false).count
    @comandas_concluidas = @comandas.where(status: true).count
    @pedidos = @comandas.sum{|a| a.orders.size}
    @pagamentos =  OrderPayment.where("comanda_id in (?)",@comandas.pluck(:id))
    orders = Order.where("comanda_id in (?)",@comandas.pluck(:id))
    @product_orders = ProductOrder.select("flavor_size_id").where("status <> 'cancelado' and order_id in (?) ",orders.pluck(:id)).order(2).group(:flavor_size_id).sum(:quantity)
    @mais_pedidos = FlavorSize.all.sort_by{|a| a.product_orders.sum(:quantity)}[1..5]
  end

  # GET /comandas/new
  def new
    if params[:table].present?
      @comanda = Comanda.new(dining_table_id: params[:table], tax: 0,fechada: false)
    else
      @comanda = Comanda.new(tax: 0,fechada: false)
    end

  end

  def  novo_pedido
    
  end

  # GET /comandas/1/edit
  def edit
  end

  # POST /comandas
  def create
    @comanda = Comanda.new(comanda_params)
    @comanda.status = false
    if @comanda.save
      if !params[:balcao].present?
        redirect_to novo_pedido_comanda_path(@comanda), notice: 'Comanda criada com sucesso!'
      else
        redirect_to @comanda
      end

    else
      render :new
    end
  end



  # PATCH/PUT /comandas/1
  def update

    if @comanda.update(comanda_params)
      redirect_to @comanda, notice: 'Comanda atualizada com sucesso!'
    else
      render :edit
    end
  end

  def fechar
    @comanda.fechada = params[:status]
    if params[:status] == "false"#verifica se está reabrindo para então remover o status de paga
      @comanda.status = false
    end
    @comanda.who_close = current_user.id
    @comanda.save
    @comanda.print_cloud
    redirect_to @comanda
  end

  def concluir
    @comanda.status = true
    @comanda.fechada = true
    if @comanda.save
      @comanda.orders.where("status <> 'cancelado' ").update(status: :concluido)
      @comanda.product_orders.where("product_orders.status <> 'cancelado' ").update(status: :concluido)
      @comanda.sync_contaazul
      @comanda.print_cloud
      if @comanda.dining_table.present?
        redirect_to comandas_dining_table_path(@comanda.dining_table)
      else
        redirect_to root_path
      end
    else
      redirect_to comanda_path(@comanda), alert: @comanda.errors.full_messages.to_sentence
    end



  end

  # DELETE /comandas/1
  def destroy
    @comanda.destroy
    redirect_to comandas_url, notice: 'Comanda excluída com sucesso!'
  end

  private
  def set_combos
    @clients = Client.all.sort_by{|e| e.nome}.map{|a| [a,a.id]}
    @payments = PaymentType.all.sort_by{|e| e.name}.map{|a| [a,a.id]}
    @flavors = FlavorSize.includes(:flavor,:product_size,:product).where(active:true, visible:true).sort_by{|a| a.product.name}.map{|a| ["#{a.product.name} - #{a.flavor} - #{a.product_size.name}", a.id]}
    @sabores = FlavorSize.includes(:flavor,:product_size,:product).where(active: true, visible: false).sort_by{|a| a.product.name}.map{|a| ["#{a.product.name} - #{a.flavor} - #{a.product_size.name}", a.id]}
    @mesas = DiningTable.all.order("NULLIF(regexp_replace(number, '\\D', '', 'g'), '')::int").map{|a| [a.number,a.id]}

  end
    # Use callbacks to share common setup or constraints between actions.
    def set_comanda
      @comanda = Comanda.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def comanda_params
      params.require(:comanda).permit(:dining_table_id,:desconto,:fechada,:tax, :tipo, :entrega,:client_id,:couvert_value, :couvert_quantity,
      orders_attributes:[:id,:comanda_id,:status,:delivery_time,:user_id,:_destroy,
        product_orders_attributes:[:id,:user_id,:flavor_size_id, :order_id, :quantity, :note, :status, :value, :_destroy,
          flavor_orders_attributes:[:id,:product_order_id,:flavor_size_id, :_destroy] ] ],
      order_payments_attributes:[:id,:payment_type_id,:value,:client_id,:paid,:comanda_id,:_destroy])
    end
end
