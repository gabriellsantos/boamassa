class OrdersController < ApplicationController
  before_action :authenticate_user!, except: [:create, :print]
  load_and_authorize_resource  except: [:print]
  skip_before_action :verify_authenticity_token
  before_action :set_order, only: [:show,:edit, :update, :destroy,:change_comanda,:cancelar, :print]

  # GET /orders
  def index
    @q = Order.all.ransack(params[:q])
    @orders = @q.result.page(params[:page])
  end

  # GET /orders/1
  def show
  end

  # GET /orders/new
  def new
    @order = Order.new
  end

  # GET /orders/1/edit
  def edit
  end

  def change_comanda
    comanda = Comanda.find(params[:comanda])
    @order.comanda = comanda
    @order.save
    redirect_to @order.comanda
  end

  def cancelar
    @order.status = :cancelado
    @order.cancel_reason_id = params[:cancel_reason_id]
    @order.save
    redirect_to @order.comanda
  end

  # POST /orders
  def create

    @order = Order.new(order_params)
    @order.status = :fila
    @order.user = current_user

    if user_signed_in? or @customer_app.present?
      saved = @order.save
    else
      saved = false
    end

    respond_to do |format|
      if saved
        format.html{redirect_to @order, notice: 'Order was successfully created.'}
        format.json{render json: {msg:"saved", path: comanda_path(@order.comanda)}}
      else
        format.html{render :new}
        format.json{render json:{msg:"error"}}
      end
    end

  end

  # PATCH/PUT /orders/1
  def update
    if @order.update(order_params)
      redirect_to @order, notice: 'Order was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /orders/1
  def destroy
    @order.destroy
    redirect_to orders_url, notice: 'Order was successfully destroyed.'
  end

  def print
    @area = Area.find(params[:area])
    @product_orders = @area.product_orders.where(order: @order)
    respond_to do |format|
      format.html{ render layout: false }
      format.text
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def order_params
      params.require(:order).permit(:comanda_id, :status, :delivery_time, :delivery_duration,:cancel_reason_id,:user_id,
                                    product_orders_attributes:[:id,:flavor_size_id,:user_id, :order_id, :quantity, :note, :status, :value, :_destroy,
                                    flavor_orders_attributes:[:id,:product_order_id,:flavor_size_id, :_destroy],
                                    pedido_extras_attributes:[:id,:product_order_id,:extra_id,:value,:_destroy]])
    end
end
