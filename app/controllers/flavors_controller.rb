class FlavorsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_flavor, only: [:show, :edit, :update, :destroy]
  before_action :set_combos, only: [:new, :edit, :create]

  # GET /flavors
  def index
    @q = Flavor.all.ransack(params[:q])
    @flavors = @q.result.page(params[:page])
  end

  # GET /flavors/1
  def show
  end

  # GET /flavors/new
  def new
    @flavor = Flavor.new

  end

  # GET /flavors/1/edit
  def edit

  end

  # POST /flavors
  def create
    @flavor = Flavor.new(flavor_params)

    if @flavor.save
      redirect_to @flavor, notice: 'Produto de Saída criado com sucesso!'
    else
      render :new
    end
  end

  # PATCH/PUT /flavors/1
  def update
    if @flavor.update(flavor_params)
      redirect_to @flavor, notice: 'Produto de Saída atualizado com sucesso!'
    else
      render :edit
    end
  end

  # DELETE /flavors/1
  def destroy
    begin
      @flavor.destroy
      redirect_to flavors_url, notice: 'Produto de Saída excluído com sucesso!'
    rescue
      redirect_to flavors_url, alert: 'Produto de Saída já está vinculado!'
    end

  end

  private
  def set_combos
    @ingredients = Ingredient.all.sort_by{|e| e.name }.map{|a| [a.name,a.id]}
    @products = Product.all.sort_by{|e| e.name }.map{|a| [a.name,a.id]}
  end
  
    # Use callbacks to share common setup or constraints between actions.
    def set_flavor
      @flavor = Flavor.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def flavor_params
      params.require(:flavor).permit(:product_id,:name, :description,ingredient_flavors_attributes:[:id,:ingredient_id,:flavor_id,:quantity,:_destroy])
    end
end
