class PromotionFlavorSizesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_promotion_flavor_size, only: [:show, :edit, :update, :destroy]

  # GET /promotion_flavor_sizes
  def index
    @q = PromotionFlavorSize.all.ransack(params[:q])
    @promotion_flavor_sizes = @q.result.page(params[:page])
  end

  # GET /promotion_flavor_sizes/1
  def show
  end

  # GET /promotion_flavor_sizes/new
  def new
    @promotion_flavor_size = PromotionFlavorSize.new
  end

  # GET /promotion_flavor_sizes/1/edit
  def edit
  end

  # POST /promotion_flavor_sizes
  def create
    @promotion_flavor_size = PromotionFlavorSize.new(promotion_flavor_size_params)

    if @promotion_flavor_size.save
      redirect_to @promotion_flavor_size, notice: 'Promotion flavor size was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /promotion_flavor_sizes/1
  def update
    if @promotion_flavor_size.update(promotion_flavor_size_params)
      redirect_to @promotion_flavor_size, notice: 'Promotion flavor size was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /promotion_flavor_sizes/1
  def destroy
    @promotion_flavor_size.destroy
    redirect_to promotion_flavor_sizes_url, notice: 'Promotion flavor size was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_promotion_flavor_size
      @promotion_flavor_size = PromotionFlavorSize.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def promotion_flavor_size_params
      params.require(:promotion_flavor_size).permit(:discount, :promotion_id, :flavor_size_id)
    end
end
