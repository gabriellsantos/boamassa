class CouvertsController < ApplicationController
  load_and_authorize_resource
  before_action :set_couvert, only: [:show, :edit, :update, :destroy]

  # GET /couverts
  def index
    @q = Couvert.all.ransack(params[:q])
    @couverts = @q.result.page(params[:page])
  end

  # GET /couverts/1
  def show
  end

  # GET /couverts/new
  def new
    @couvert = Couvert.new
  end

  # GET /couverts/1/edit
  def edit
  end

  # POST /couverts
  def create
    @couvert = Couvert.new(couvert_params)

    if @couvert.save
      redirect_to @couvert, notice: 'Couvert was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /couverts/1
  def update
    if @couvert.update(couvert_params)
      redirect_to @couvert, notice: 'Couvert was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /couverts/1
  def destroy
    @couvert.destroy
    redirect_to couverts_url, notice: 'Couvert was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_couvert
      @couvert = Couvert.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def couvert_params
      params.require(:couvert).permit(:name, :value)
    end
end
