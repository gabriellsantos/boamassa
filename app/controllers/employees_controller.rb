class EmployeesController < ApplicationController
  before_action :set_employee, only: [:show, :edit, :update, :destroy, :adiantamento]

  # GET /employees
  def index
    @q = Employee.all.ransack(params[:q])
    @employees = @q.result.page(params[:page])
  end

  # GET /employees/1
  def show
  end

  def imprimir_adiantamento
    @pagamento = DownPayment.find(params[:pagamento])
    respond_to do |format|
      format.html{render layout: false}
      format.text
    end
  end

  def deletar_vale
    vale = DownPayment.find(params[:vale])
    vale.destroy
    redirect_to vale.employee
  end

  # GET /employees/new
  def new
    @employee = Employee.new
  end

  def adiantamento
    DownPayment.create(value: params[:valor], data: params[:data], employee: @employee, note: params[:note])
    redirect_to @employee
  end

  # GET /employees/1/edit
  def edit
  end

  # POST /employees
  def create
    @employee = Employee.new(employee_params)

    if @employee.save
      redirect_to @employee, notice: 'Funcionário criado com sucesso!'
    else
      render :new
    end
  end

  # PATCH/PUT /employees/1
  def update
    if @employee.update(employee_params)
      redirect_to @employee, notice: 'Funcionário atualizado com sucesso!'
    else
      render :edit
    end
  end

  # DELETE /employees/1
  def destroy
    begin
      @employee.destroy
      redirect_to employees_url, notice: 'Funcionário excluído com sucesso'
    rescue
      redirect_to employees_url, alert: 'Funcionário já está vinculado!'
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def employee_params
      params.require(:employee).permit(:name, :address, :pis, :phone, :cpf, :rg, :hiring_date, :demission_date)
    end
end
