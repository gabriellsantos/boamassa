class AreasController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_area, only: [:show, :edit, :update, :destroy]

  # GET /areas
  def index
    @q = Area.all.ransack(params[:q])
    @areas = @q.result.page(params[:page])
  end

  def atualiza
    render js: "window.location = '/areas/#{params[:area]}'"
  end

  # GET /areas/1
  def show
    @products = ProductOrder.includes(:product, :order).where("product_orders.status <> 'concluido' and product_orders.status <> 'cancelado' ").where(products:{area: @area}).group_by(&:order)
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /areas/new
  def new
    @area = Area.new
  end

  # GET /areas/1/edit
  def edit
  end

  # POST /areas
  def create
    @area = Area.new(area_params)

    if @area.save
      redirect_to @area, notice: 'Área criada com sucesso'
    else
      render :new
    end
  end

  # PATCH/PUT /areas/1
  def update
    if @area.update(area_params)
      redirect_to @area, notice: 'Área atualizada com sucesso'
    else
      render :edit
    end
  end

  # DELETE /areas/1
  def destroy
    @area.destroy
    redirect_to areas_url, notice: 'Area excluída com sucesso'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_area
      @area = Area.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def area_params
      params.require(:area).permit(:name, :printer)
    end
end
