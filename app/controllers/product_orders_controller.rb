class ProductOrdersController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_product_order, only: [:show, :edit, :update, :destroy,:change_comanda,:cancelar,:imprimir,:concluir]

  # GET /product_orders
  def index
    @q = ProductOrder.all.ransack(params[:q])
    @product_orders = @q.result.page(params[:page])
  end

  # GET /product_orders/1
  def show
  end

  # GET /product_orders/new
  def new
    @product_order = ProductOrder.new
  end

  def cancelar
    @product_order.status = :cancelado
    @product_order.cancel_reason_id = params[:cancel_reason_id]
    @product_order.tempo = (Time.now - @product_order.created_at)/60
    @product_order.save
    redirect_to @product_order.comanda
  end

  def concluir
    @product_order.status = :concluido
    @product_order.tempo = (Time.now - @product_order.created_at)/60
    @product_order.save

    redirect_to @product_order.comanda
  end

  def imprimir
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pedido"
      end
    end
  end

  def change_comanda
   comanda = Comanda.find(params[:comanda])
   order = Order.create(comanda: comanda)
   @product_order.order = order
   @product_order.save
    redirect_to @product_order.comanda

  end

  # GET /product_orders/1/edit
  def edit
  end

  def atualiza_pedido
    produto = ProductOrder.find(params[:produto])
    produto.status = params[:status]
    produto.save
    redirect_to area_path(params[:area])
  end

  # POST /product_orders
  def create
    @product_order = ProductOrder.new(product_order_params)

    if @product_order.save
      redirect_to @product_order, notice: 'Product order was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /product_orders/1
  def update
    if @product_order.update(product_order_params)
      redirect_to @product_order, notice: 'Product order was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /product_orders/1
  def destroy
    @product_order.destroy
    redirect_to product_orders_url, notice: 'Product order was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_order
      @product_order = ProductOrder.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_order_params
      params.require(:product_order).permit(:order_id, :quantity, :note, :status, :value)
    end
end
