# == Schema Information
#
# Table name: flavor_sizes
#
#  id              :integer          not null, primary key
#  flavor_id       :integer
#  product_size_id :integer
#  value           :decimal(, )
#  active          :boolean
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  visible         :boolean
#
# Indexes
#
#  index_flavor_sizes_on_flavor_id        (flavor_id)
#  index_flavor_sizes_on_product_size_id  (product_size_id)
#
# Foreign Keys
#
#  fk_rails_...  (flavor_id => flavors.id)
#  fk_rails_...  (product_size_id => product_sizes.id)
#

require 'test_helper'

class FlavorSizeTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
