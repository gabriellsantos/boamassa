# == Schema Information
#
# Table name: product_orders
#
#  id               :integer          not null, primary key
#  order_id         :integer
#  note             :string
#  status           :string
#  value            :decimal(, )
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  flavor_size_id   :integer
#  quantity         :decimal(, )
#  cancel_reason_id :integer
#  tempo            :string
#  user_id          :integer
#
# Indexes
#
#  index_product_orders_on_cancel_reason_id  (cancel_reason_id)
#  index_product_orders_on_flavor_size_id    (flavor_size_id)
#  index_product_orders_on_order_id          (order_id)
#  index_product_orders_on_user_id           (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (cancel_reason_id => cancel_reasons.id)
#  fk_rails_...  (flavor_size_id => flavor_sizes.id)
#  fk_rails_...  (order_id => orders.id)
#  fk_rails_...  (user_id => users.id)
#

require 'test_helper'

class ProductOrderTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
