# == Schema Information
#
# Table name: extras
#
#  id              :integer          not null, primary key
#  product_size_id :integer
#  flavor_id       :integer
#  value           :decimal(, )
#  active          :boolean
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_extras_on_flavor_id        (flavor_id)
#  index_extras_on_product_size_id  (product_size_id)
#
# Foreign Keys
#
#  fk_rails_...  (flavor_id => flavors.id)
#  fk_rails_...  (product_size_id => product_sizes.id)
#

require 'test_helper'

class ExtraTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
