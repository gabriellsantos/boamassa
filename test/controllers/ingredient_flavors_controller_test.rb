require 'test_helper'

class IngredientFlavorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ingredient_flavor = ingredient_flavors(:one)
  end

  test "should get index" do
    get ingredient_flavors_url
    assert_response :success
  end

  test "should get new" do
    get new_ingredient_flavor_url
    assert_response :success
  end

  test "should create ingredient_flavor" do
    assert_difference('IngredientFlavor.count') do
      post ingredient_flavors_url, params: { ingredient_flavor: { flavor_id: @ingredient_flavor.flavor_id, ingredient_id: @ingredient_flavor.ingredient_id } }
    end

    assert_redirected_to ingredient_flavor_url(IngredientFlavor.last)
  end

  test "should show ingredient_flavor" do
    get ingredient_flavor_url(@ingredient_flavor)
    assert_response :success
  end

  test "should get edit" do
    get edit_ingredient_flavor_url(@ingredient_flavor)
    assert_response :success
  end

  test "should update ingredient_flavor" do
    patch ingredient_flavor_url(@ingredient_flavor), params: { ingredient_flavor: { flavor_id: @ingredient_flavor.flavor_id, ingredient_id: @ingredient_flavor.ingredient_id } }
    assert_redirected_to ingredient_flavor_url(@ingredient_flavor)
  end

  test "should destroy ingredient_flavor" do
    assert_difference('IngredientFlavor.count', -1) do
      delete ingredient_flavor_url(@ingredient_flavor)
    end

    assert_redirected_to ingredient_flavors_url
  end
end
