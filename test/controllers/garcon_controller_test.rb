require 'test_helper'

class GarconControllerTest < ActionDispatch::IntegrationTest
  test "should get chamadas" do
    get garcon_chamadas_url
    assert_response :success
  end

  test "should get pedido" do
    get garcon_pedido_url
    assert_response :success
  end

  test "should get fechar_conta" do
    get garcon_fechar_conta_url
    assert_response :success
  end

end
