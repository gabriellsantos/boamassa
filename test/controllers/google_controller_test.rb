require 'test_helper'

class GoogleControllerTest < ActionDispatch::IntegrationTest
  test "should get callback" do
    get google_callback_url
    assert_response :success
  end

  test "should get redirect" do
    get google_redirect_url
    assert_response :success
  end

  test "should get jobs" do
    get google_jobs_url
    assert_response :success
  end

end
