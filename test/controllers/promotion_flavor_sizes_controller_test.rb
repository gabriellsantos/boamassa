require 'test_helper'

class PromotionFlavorSizesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @promotion_flavor_size = promotion_flavor_sizes(:one)
  end

  test "should get index" do
    get promotion_flavor_sizes_url
    assert_response :success
  end

  test "should get new" do
    get new_promotion_flavor_size_url
    assert_response :success
  end

  test "should create promotion_flavor_size" do
    assert_difference('PromotionFlavorSize.count') do
      post promotion_flavor_sizes_url, params: { promotion_flavor_size: { discount: @promotion_flavor_size.discount, flavor_size_id: @promotion_flavor_size.flavor_size_id, promotion_id: @promotion_flavor_size.promotion_id } }
    end

    assert_redirected_to promotion_flavor_size_url(PromotionFlavorSize.last)
  end

  test "should show promotion_flavor_size" do
    get promotion_flavor_size_url(@promotion_flavor_size)
    assert_response :success
  end

  test "should get edit" do
    get edit_promotion_flavor_size_url(@promotion_flavor_size)
    assert_response :success
  end

  test "should update promotion_flavor_size" do
    patch promotion_flavor_size_url(@promotion_flavor_size), params: { promotion_flavor_size: { discount: @promotion_flavor_size.discount, flavor_size_id: @promotion_flavor_size.flavor_size_id, promotion_id: @promotion_flavor_size.promotion_id } }
    assert_redirected_to promotion_flavor_size_url(@promotion_flavor_size)
  end

  test "should destroy promotion_flavor_size" do
    assert_difference('PromotionFlavorSize.count', -1) do
      delete promotion_flavor_size_url(@promotion_flavor_size)
    end

    assert_redirected_to promotion_flavor_sizes_url
  end
end
