require 'test_helper'

class DiningTablesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @dining_table = dining_tables(:one)
  end

  test "should get index" do
    get dining_tables_url
    assert_response :success
  end

  test "should get new" do
    get new_dining_table_url
    assert_response :success
  end

  test "should create dining_table" do
    assert_difference('DiningTable.count') do
      post dining_tables_url, params: { dining_table: { beacon: @dining_table.beacon, number: @dining_table.number, status: @dining_table.status } }
    end

    assert_redirected_to dining_table_url(DiningTable.last)
  end

  test "should show dining_table" do
    get dining_table_url(@dining_table)
    assert_response :success
  end

  test "should get edit" do
    get edit_dining_table_url(@dining_table)
    assert_response :success
  end

  test "should update dining_table" do
    patch dining_table_url(@dining_table), params: { dining_table: { beacon: @dining_table.beacon, number: @dining_table.number, status: @dining_table.status } }
    assert_redirected_to dining_table_url(@dining_table)
  end

  test "should destroy dining_table" do
    assert_difference('DiningTable.count', -1) do
      delete dining_table_url(@dining_table)
    end

    assert_redirected_to dining_tables_url
  end
end
