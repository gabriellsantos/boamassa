require 'test_helper'

class CancelReasonsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cancel_reason = cancel_reasons(:one)
  end

  test "should get index" do
    get cancel_reasons_url
    assert_response :success
  end

  test "should get new" do
    get new_cancel_reason_url
    assert_response :success
  end

  test "should create cancel_reason" do
    assert_difference('CancelReason.count') do
      post cancel_reasons_url, params: { cancel_reason: { name: @cancel_reason.name } }
    end

    assert_redirected_to cancel_reason_url(CancelReason.last)
  end

  test "should show cancel_reason" do
    get cancel_reason_url(@cancel_reason)
    assert_response :success
  end

  test "should get edit" do
    get edit_cancel_reason_url(@cancel_reason)
    assert_response :success
  end

  test "should update cancel_reason" do
    patch cancel_reason_url(@cancel_reason), params: { cancel_reason: { name: @cancel_reason.name } }
    assert_redirected_to cancel_reason_url(@cancel_reason)
  end

  test "should destroy cancel_reason" do
    assert_difference('CancelReason.count', -1) do
      delete cancel_reason_url(@cancel_reason)
    end

    assert_redirected_to cancel_reasons_url
  end
end
