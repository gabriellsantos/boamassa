require 'test_helper'

class FlavorOrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @flavor_order = flavor_orders(:one)
  end

  test "should get index" do
    get flavor_orders_url
    assert_response :success
  end

  test "should get new" do
    get new_flavor_order_url
    assert_response :success
  end

  test "should create flavor_order" do
    assert_difference('FlavorOrder.count') do
      post flavor_orders_url, params: { flavor_order: { flavor_size_id: @flavor_order.flavor_size_id, product_order_id: @flavor_order.product_order_id } }
    end

    assert_redirected_to flavor_order_url(FlavorOrder.last)
  end

  test "should show flavor_order" do
    get flavor_order_url(@flavor_order)
    assert_response :success
  end

  test "should get edit" do
    get edit_flavor_order_url(@flavor_order)
    assert_response :success
  end

  test "should update flavor_order" do
    patch flavor_order_url(@flavor_order), params: { flavor_order: { flavor_size_id: @flavor_order.flavor_size_id, product_order_id: @flavor_order.product_order_id } }
    assert_redirected_to flavor_order_url(@flavor_order)
  end

  test "should destroy flavor_order" do
    assert_difference('FlavorOrder.count', -1) do
      delete flavor_order_url(@flavor_order)
    end

    assert_redirected_to flavor_orders_url
  end
end
