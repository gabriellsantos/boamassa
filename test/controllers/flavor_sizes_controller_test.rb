require 'test_helper'

class FlavorSizesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @flavor_size = flavor_sizes(:one)
  end

  test "should get index" do
    get flavor_sizes_url
    assert_response :success
  end

  test "should get new" do
    get new_flavor_size_url
    assert_response :success
  end

  test "should create flavor_size" do
    assert_difference('FlavorSize.count') do
      post flavor_sizes_url, params: { flavor_size: { active: @flavor_size.active, flavor_id: @flavor_size.flavor_id, product_size_id: @flavor_size.product_size_id, value: @flavor_size.value } }
    end

    assert_redirected_to flavor_size_url(FlavorSize.last)
  end

  test "should show flavor_size" do
    get flavor_size_url(@flavor_size)
    assert_response :success
  end

  test "should get edit" do
    get edit_flavor_size_url(@flavor_size)
    assert_response :success
  end

  test "should update flavor_size" do
    patch flavor_size_url(@flavor_size), params: { flavor_size: { active: @flavor_size.active, flavor_id: @flavor_size.flavor_id, product_size_id: @flavor_size.product_size_id, value: @flavor_size.value } }
    assert_redirected_to flavor_size_url(@flavor_size)
  end

  test "should destroy flavor_size" do
    assert_difference('FlavorSize.count', -1) do
      delete flavor_size_url(@flavor_size)
    end

    assert_redirected_to flavor_sizes_url
  end
end
