require 'test_helper'

class TaghereControllerTest < ActionDispatch::IntegrationTest
  test "should get menu" do
    get taghere_menu_url
    assert_response :success
  end

  test "should get set_table" do
    get taghere_set_table_url
    assert_response :success
  end

  test "should get request" do
    get taghere_request_url
    assert_response :success
  end

  test "should get balance" do
    get taghere_balance_url
    assert_response :success
  end

end
