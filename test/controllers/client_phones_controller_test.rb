require 'test_helper'

class ClientPhonesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @client_phone = client_phones(:one)
  end

  test "should get index" do
    get client_phones_url
    assert_response :success
  end

  test "should get new" do
    get new_client_phone_url
    assert_response :success
  end

  test "should create client_phone" do
    assert_difference('ClientPhone.count') do
      post client_phones_url, params: { client_phone: { client_id: @client_phone.client_id, phone: @client_phone.phone } }
    end

    assert_redirected_to client_phone_url(ClientPhone.last)
  end

  test "should show client_phone" do
    get client_phone_url(@client_phone)
    assert_response :success
  end

  test "should get edit" do
    get edit_client_phone_url(@client_phone)
    assert_response :success
  end

  test "should update client_phone" do
    patch client_phone_url(@client_phone), params: { client_phone: { client_id: @client_phone.client_id, phone: @client_phone.phone } }
    assert_redirected_to client_phone_url(@client_phone)
  end

  test "should destroy client_phone" do
    assert_difference('ClientPhone.count', -1) do
      delete client_phone_url(@client_phone)
    end

    assert_redirected_to client_phones_url
  end
end
