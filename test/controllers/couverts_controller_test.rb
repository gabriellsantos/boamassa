require 'test_helper'

class CouvertsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @couvert = couverts(:one)
  end

  test "should get index" do
    get couverts_url
    assert_response :success
  end

  test "should get new" do
    get new_couvert_url
    assert_response :success
  end

  test "should create couvert" do
    assert_difference('Couvert.count') do
      post couverts_url, params: { couvert: { name: @couvert.name, value: @couvert.value } }
    end

    assert_redirected_to couvert_url(Couvert.last)
  end

  test "should show couvert" do
    get couvert_url(@couvert)
    assert_response :success
  end

  test "should get edit" do
    get edit_couvert_url(@couvert)
    assert_response :success
  end

  test "should update couvert" do
    patch couvert_url(@couvert), params: { couvert: { name: @couvert.name, value: @couvert.value } }
    assert_redirected_to couvert_url(@couvert)
  end

  test "should destroy couvert" do
    assert_difference('Couvert.count', -1) do
      delete couvert_url(@couvert)
    end

    assert_redirected_to couverts_url
  end
end
