require 'test_helper'

class IngredientEntracesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ingredient_entrace = ingredient_entraces(:one)
  end

  test "should get index" do
    get ingredient_entraces_url
    assert_response :success
  end

  test "should get new" do
    get new_ingredient_entrace_url
    assert_response :success
  end

  test "should create ingredient_entrace" do
    assert_difference('IngredientEntrace.count') do
      post ingredient_entraces_url, params: { ingredient_entrace: { entrance_id: @ingredient_entrace.entrance_id, ingredient_id: @ingredient_entrace.ingredient_id, value: @ingredient_entrace.value } }
    end

    assert_redirected_to ingredient_entrace_url(IngredientEntrace.last)
  end

  test "should show ingredient_entrace" do
    get ingredient_entrace_url(@ingredient_entrace)
    assert_response :success
  end

  test "should get edit" do
    get edit_ingredient_entrace_url(@ingredient_entrace)
    assert_response :success
  end

  test "should update ingredient_entrace" do
    patch ingredient_entrace_url(@ingredient_entrace), params: { ingredient_entrace: { entrance_id: @ingredient_entrace.entrance_id, ingredient_id: @ingredient_entrace.ingredient_id, value: @ingredient_entrace.value } }
    assert_redirected_to ingredient_entrace_url(@ingredient_entrace)
  end

  test "should destroy ingredient_entrace" do
    assert_difference('IngredientEntrace.count', -1) do
      delete ingredient_entrace_url(@ingredient_entrace)
    end

    assert_redirected_to ingredient_entraces_url
  end
end
