require 'test_helper'

class PedidoExtrasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pedido_extra = pedido_extras(:one)
  end

  test "should get index" do
    get pedido_extras_url
    assert_response :success
  end

  test "should get new" do
    get new_pedido_extra_url
    assert_response :success
  end

  test "should create pedido_extra" do
    assert_difference('PedidoExtra.count') do
      post pedido_extras_url, params: { pedido_extra: { extra_id: @pedido_extra.extra_id, product_order_id: @pedido_extra.product_order_id, value: @pedido_extra.value } }
    end

    assert_redirected_to pedido_extra_url(PedidoExtra.last)
  end

  test "should show pedido_extra" do
    get pedido_extra_url(@pedido_extra)
    assert_response :success
  end

  test "should get edit" do
    get edit_pedido_extra_url(@pedido_extra)
    assert_response :success
  end

  test "should update pedido_extra" do
    patch pedido_extra_url(@pedido_extra), params: { pedido_extra: { extra_id: @pedido_extra.extra_id, product_order_id: @pedido_extra.product_order_id, value: @pedido_extra.value } }
    assert_redirected_to pedido_extra_url(@pedido_extra)
  end

  test "should destroy pedido_extra" do
    assert_difference('PedidoExtra.count', -1) do
      delete pedido_extra_url(@pedido_extra)
    end

    assert_redirected_to pedido_extras_url
  end
end
